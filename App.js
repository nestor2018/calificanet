'use strict';
import React, {useEffect} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import messaging from '@react-native-firebase/messaging';
import {Toast} from '@kyupss/native-popup';

import {store, persistor} from './src/store';
import StackVavigation from './src/navigation/stackNavigation';

const App = () => {
  useEffect(() => {
    const foregroundSubscribe = messaging().onMessage(async remoteMessage => {
      console.log('notificacion recivida', remoteMessage);
      Toast.show({
        title: remoteMessage.notification.title,
        text: remoteMessage.notification.body,
        color: 'green',
        position: 'top', // bottom, center, top
      });
    });

    const backgroundSubscribe = messaging().setBackgroundMessageHandler(
      async remoteMessage => {
        console.log('push notification en background', remoteMessage);
      },
    );

    return () => {
      foregroundSubscribe();
      backgroundSubscribe();
    };
  }, []);
  return (
    <NavigationContainer>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <StackVavigation />
        </PersistGate>
      </Provider>
    </NavigationContainer>
  );
};

export default App;
