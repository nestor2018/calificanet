import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  ScrollView,
} from 'react-native';

const WIDTH = Dimensions.get('window').width;
const HEIGHT = Dimensions.get('window').height;
const ModalPiker = props => {
  console.log(props);
  const onPressItem = item => {
    props.changeModalVisible(false);
    props.setData(item);
  };

  const dataOptions = item => {
    if (props.typeOptions == 'colegios') {
      return item.str_col_nombre;
    } else if (props.typeOptions == 'perfiles') {
      return item.str_cpe_perfil;
    } else if (props.typeOptions == 'students') {
      return item.persona.str_prs_nombre_completo;
    } else if (props.typeOptions == 'periods') {
      return item.periodo;
    } else if (props.typeOptions == 'perfilMessage') {
      return item.str_per_nombre;
    } else if (props.typeOptions == 'perfilDestinatario') {
      return item.nombre;
    }
  };

  const options = props.options.map((item, index) => {
    return (
      <TouchableOpacity
        style={styles.option}
        key={index}
        onPress={() => onPressItem(item)}>
        <Text style={styles.text}>{dataOptions(item)}</Text>
      </TouchableOpacity>
    );
  });
  return (
    <TouchableOpacity
      onPress={() => props.changeModalVisible(false)}
      style={styles.container}>
      <View style={[styles.modal, {width: WIDTH - 20, height: HEIGHT / 2}]}>
        <ScrollView>{options}</ScrollView>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modal: {
    backgroundColor: 'white',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: 'black',
  },
  option: {
    alignItems: 'flex-start',
  },
  text: {
    margin: 20,
    fontSize: 20,
    fontWeight: 'bold',
  },
});

export default ModalPiker;
