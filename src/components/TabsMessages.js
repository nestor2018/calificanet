import React, {useEffect} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import colors from '../constants/colors';
import {changeTypeGrid, received} from '../store/allData';
import {messagges, buzonrequest} from '../store/messagges';

const TabsMessages = () => {
  const dispatch = useDispatch();

  const token = useSelector(state => state.user.token);
  const url = useSelector(state => state.colegioSelected.url);
  const uid_per = useSelector(state => state.user.user.info_usuario.perfil.uid);
  const typeGrid = useSelector(state => state.allData.typeGrid);
  useEffect(() => {
    dispatch(changeTypeGrid('receivedMessages'));
  }, []);

  let getBuzon = () => {
    console.log('entra');
    dispatch(
      buzonrequest({
        url,
        token,
      }),
    );
  };
  const changeMessage = typeMessage => {
    dispatch(changeTypeGrid(typeMessage));
  };

  return (
    <View style={styles.container} opacity={0.7}>
      <TouchableOpacity onPress={() => changeMessage('receivedMessages')}>
        {typeGrid == 'receivedMessages' ? (
          <Text style={[styles.text, styles.bold]}>ENTRADA</Text>
        ) : (
          <Text style={styles.text}>ENTRADA</Text>
        )}
      </TouchableOpacity>
      <TouchableOpacity onPress={() => changeMessage('sendMessages')}>
        {typeGrid == 'sendMessages' ? (
          <Text style={[styles.text, styles.bold]}>SALIDA</Text>
        ) : (
          <Text style={styles.text}>SALIDA</Text>
        )}
      </TouchableOpacity>
      <TouchableOpacity onPress={() => changeMessage('paperMessages')}>
        {typeGrid == 'paperMessages' ? (
          <Text style={[styles.text, styles.bold]}>PAPELERA</Text>
        ) : (
          <Text style={styles.text}>PAPELERA</Text>
        )}
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    backgroundColor: colors.messagge,
    padding: 20,
  },
  text: {
    color: colors.white,
  },
  bold: {
    fontWeight: 'bold',
  },
});

export default TabsMessages;
