import React, {Component} from 'react';
import {WebView} from 'react-native-webview';

export const Webview = props => {
  return <WebView originWhitelist={['*']} source={{uri: props.url}} />;
};
