import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Entypo from 'react-native-vector-icons/Entypo';

import colors from '../constants/colors';

const HeaderApp = props => {
  return (
    <View style={styles.container}>
      <FontAwesome5 name={'bars'} size={22} onPress={props.openDrawer} />
      <Text>{props.title}</Text>
      <Entypo name={'dots-three-vertical'} size={22} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.blue,
    padding: 20,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
});

export default HeaderApp;
