import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  Animated,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Swipeable from 'react-native-gesture-handler/Swipeable';
import {RectButton} from 'react-native-gesture-handler';

import ItemSeparator from '../components/ItemSeparator';
import {selectedMessage, allData} from '../store/allData';
import {
  deleteMessagges,
  restaurarMessagges,
  messagges,
  papeleraMesage,
  eliminarPapeleraStore,
} from '../store/messagges';
import colors from '../constants/colors';

const Grid = props => {
  const token = useSelector(state => state.user.token);
  const url = useSelector(state => state.colegioSelected.url);
  const uid_per = useSelector(state => state.user.user.info_usuario.perfil.uid);
  const [selected, setSelected] = useState({});

  const eliminar = uid => {
    dispatch(deleteMessagges({url, token, uids: [uid]}));
    dispatch(
      messagges({
        url,
        token,
        uid_per,
      }),
    );
    dispatch(papeleraMesage({url, token}));
  };

  const restaurar = uid => {
    dispatch(restaurarMessagges({url, token, uids: [uid]}));
    dispatch(
      messagges({
        url,
        token,
        uid_per,
      }),
    );
    dispatch(papeleraMesage({url, token}));
  };

  const eliminalPapelera = uid => {
    dispatch(eliminarPapeleraStore({url, token, uids: [uid]}));
    dispatch(
      messagges({
        url,
        token,
        uid_per,
      }),
    );
    dispatch(papeleraMesage({url, token}));
  };

  const RightActions = ({progress, dragX, onPress, item}) => {
    const trans = dragX.interpolate({
      inputRange: [-100, 0],
      outputRange: [0.7, 0],
      extrapolate: 'clamp',
    });

    return (
      <TouchableOpacity onPress={() => eliminar(item.uid)}>
        <View
          style={{flex: 1, backgroundColor: 'red', justifyContent: 'center'}}>
          <Animated.Text
            style={{
              paddingHorizontal: 10,
              fontWeight: '600',
              transform: [{translateX: trans}],
            }}>
            <FontAwesome5
              name={'trash-alt'}
              color={colors.white}
              size={22}
              style={styles.icon2}
            />
          </Animated.Text>
        </View>
      </TouchableOpacity>
    );
  };

  const RightActionsRestore = ({progress, dragX, onPress, item}) => {
    const trans = dragX.interpolate({
      inputRange: [-100, 0],
      outputRange: [0.7, 0],
      extrapolate: 'clamp',
    });

    return (
      <TouchableOpacity onPress={() => restaurar(item.uid)}>
        <View
          style={{
            flex: 1,
            backgroundColor: '#00C851',
            justifyContent: 'center',
          }}>
          <Animated.Text
            style={{
              paddingHorizontal: 10,
              fontWeight: '600',
              transform: [{translateX: trans}],
            }}>
            <FontAwesome5
              name={'trash-restore-alt'}
              color={colors.white}
              size={22}
              style={styles.icon2}
            />
          </Animated.Text>
        </View>
      </TouchableOpacity>
    );
  };

  const LeftActionsDelete = ({progress, dragX, onPress, item}) => {
    const trans = dragX.interpolate({
      inputRange: [-100, 0],
      outputRange: [0.7, 0],
      extrapolate: 'clamp',
    });

    return (
      <TouchableOpacity onPress={() => eliminalPapelera(item.uid)}>
        <View
          style={{
            flex: 1,
            backgroundColor: 'red',
            justifyContent: 'center',
          }}>
          <Animated.Text
            style={{
              paddingHorizontal: 10,
              fontWeight: '600',
              transform: [{translateX: trans}],
            }}>
            <FontAwesome5
              name={'trash-restore-alt'}
              color={colors.white}
              size={22}
              style={styles.icon2}
            />
          </Animated.Text>
        </View>
      </TouchableOpacity>
    );
  };

  const pressBu = item => {};
  const Item = ({item, onPress, received}) => (
    <TouchableOpacity onPress={onPress}>
      {received == 'received' ? (
        <Swipeable
          onSwipeableRightOpen={() => console.log('22')}
          renderRightActions={(progress, dragX) => (
            <RightActions
              progress={progress}
              dragX={dragX}
              onPress={pressBu(item)}
              item={item}
            />
          )}>
          <View style={styles.justify}>
            <View style={styles.container}>
              <View style={styles.flex}>
                <Text>De: </Text>
                <Text style={styles.bold}>{item.remitente}</Text>
              </View>
              <View style={styles.flex}>
                <Text>Asunto: </Text>
                <Text style={styles.bold}>{item.str_bne_asunto}</Text>
              </View>
            </View>
            {item.boo_bne_confirmacion_lectura == 0 ? (
              <FontAwesome5
                name={'envelope-open'}
                color={colors.messagge}
                size={22}
                style={styles.icon}
              />
            ) : (
              <FontAwesome5
                name={'envelope'}
                style={styles.icon}
                color={colors.messagge}
                size={22}
              />
            )}
          </View>
        </Swipeable>
      ) : null}
      {received == 'send' ? (
        <Swipeable
          renderRightActions={(progress, dragX) => (
            <RightActions
              progress={progress}
              dragX={dragX}
              onPress={pressBu(item)}
              item={item}
            />
          )}>
          <View style={styles.justify}>
            <View style={styles.container}>
              <View style={styles.flex}>
                <Text>Para: </Text>
                {item.destinatarios
                  ? item.destinatarios.map(persons => (
                      <Text style={styles.bold}>{persons.nombre},</Text>
                    ))
                  : null}
              </View>
              <View style={styles.flex}>
                <Text>Asunto: </Text>
                <Text style={styles.bold}>{item.str_bns_asunto}</Text>
              </View>
            </View>
            {item.boo_bns_confirmacion_lectura == 0 ? (
              <FontAwesome5
                name={'envelope-open'}
                color={colors.messagge}
                size={22}
                style={styles.icon}
              />
            ) : (
              <FontAwesome5
                name={'envelope'}
                style={styles.icon}
                color={colors.messagge}
                size={22}
              />
            )}
          </View>
        </Swipeable>
      ) : null}
      {received == 'paper' ? (
        <Swipeable
          renderRightActions={(progress, dragX) => (
            <RightActionsRestore
              progress={progress}
              dragX={dragX}
              onPress={pressBu(item)}
              item={item}
            />
          )}
          renderLeftActions={(progress, dragX) => (
            <LeftActionsDelete
              progress={progress}
              dragX={dragX}
              onPress={pressBu(item)}
              item={item}
            />
          )}>
          <View style={styles.container}>
            <View style={styles.flex}>
              <Text>De: </Text>
              <Text style={styles.bold}>{item.remitente}</Text>
            </View>
            <View style={styles.flex}>
              <Text>Asunto: </Text>
              <Text style={styles.bold}>{item.str_bnp_asunto}</Text>
            </View>
          </View>
        </Swipeable>
      ) : null}
    </TouchableOpacity>
  );
  const dispatch = useDispatch();
  const receivedMessages = useSelector(state => state.messagges.buzon);
  const sendMessages = useSelector(state => state.messagges.enviados);
  const paperMessages = useSelector(state => state.messagges.papelera);
  const typeGrid = useSelector(state => state.allData.typeGrid);
  const messageSelect = item => {
    props.navigation.navigate('DetailMessage');
    dispatch(selectedMessage(item));
  };
  const renderItemReceived = ({item}) => (
    <Item
      item={item}
      onPress={() => messageSelect(item)}
      received={'received'}
    />
  );
  const renderItemSend = ({item}) => (
    <Item item={item} onPress={() => messageSelect(item)} received={'send'} />
  );
  const renderItemPaper = ({item}) => (
    <Item item={item} onPress={() => messageSelect(item)} received={'paper'} />
  );

  return (
    <View style={styles.containerPage}>
      {typeGrid == 'receivedMessages' ? (
        <FlatList
          data={receivedMessages}
          renderItem={renderItemReceived}
          keyExtractor={item => item.uid}
          ItemSeparatorComponent={({highlighted}) => <ItemSeparator />}
        />
      ) : null}
      {typeGrid == 'sendMessages' ? (
        <FlatList
          data={sendMessages}
          renderItem={renderItemSend}
          keyExtractor={item => item.uid}
          ItemSeparatorComponent={({highlighted}) => <ItemSeparator />}
        />
      ) : null}
      {typeGrid == 'paperMessages' ? (
        <FlatList
          data={paperMessages}
          renderItem={renderItemPaper}
          keyExtractor={item => item.uid}
          ItemSeparatorComponent={({highlighted}) => <ItemSeparator />}
        />
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  containerPage: {
    paddingBottom: 150,
  },
  container: {
    padding: 10,
  },
  item: {
    padding: 10,
  },
  flex: {
    flexDirection: 'row',
    marginTop: 10,
    flexWrap: 'wrap',
    minWidth: '80%',
    maxWidth: '80%',
  },
  bold: {
    fontWeight: 'bold',
  },
  justify: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  icon: {
    alignSelf: 'center',
  },
  icon2: {
    alignSelf: 'center',
  },
  a: {
    padding: 10,
    backgroundColor: 'red',
    color: 'white',
    height: '100%',
    alignSelf: 'center',
  },
  b: {
    padding: 10,
    backgroundColor: '#00C851',
    color: 'white',
    height: '100%',
    alignSelf: 'center',
  },
});
export default Grid;
