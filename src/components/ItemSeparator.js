import React from 'react';
import {View, StyleSheet} from 'react-native';

import colors from '../constants/colors';

const ItemSeparator = () => {
  return <View style={styles.itemSeparator}></View>;
};

const styles = StyleSheet.create({
  itemSeparator: {
    borderBottomWidth: 1,
    borderBottomColor: colors.black,
  },
});

export default ItemSeparator;
