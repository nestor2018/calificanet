import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import colors from '../constants/colors';

const HeaderDrawer = () => {
  const name = useSelector(state => state.user.nameUser);
  const colegio = useSelector(
    state => state.colegiosLogin.dataColegios[1].str_col_nombre,
  );
  const perfil = useSelector(
    state => state.user.user.info_usuario.perfil.str_per_nombre,
  );
  return (
    <View>
      <Text style={styles.title}>{colegio}</Text>
      <View style={styles.flex}>
        <FontAwesome5 name={'user'} color={colors.white} size={22} />
        <Text style={styles.text}>{name}</Text>
      </View>
      <Text style={styles.title}>PERFIL: {perfil}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {},
  flex: {
    paddingHorizontal: 5,
    flexDirection: 'row',
  },
  text: {
    paddingHorizontal: 10,
    color: colors.white,
  },
  title: {
    color: colors.white,
    paddingVertical: 10,
  },
});

export default HeaderDrawer;
