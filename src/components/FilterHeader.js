import React, {useState, useEffect} from 'react';
import {View, Text, Modal, StyleSheet, TouchableOpacity} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import ModalPiker from './ModalPiker';
import colors from '../constants/colors';
import {changeModalStudents, changeModalPeriods} from '../store/modal';
import {notes} from '../store/notes';
import {chores} from '../store/chores';
import {selectedStudent} from '../store/user';
import {selectedPeriod} from '../store/allData';
import {absence} from '../store/absence';

const FilterHeader = ({type, bgColor}) => {
  const url = useSelector(state => state.colegioSelected.url);
  const dispatch = useDispatch();
  const token = useSelector(state => state.user.token);
  const students = useSelector(state => state.user.students);
  const studentSelected = useSelector(
    state => state.user.studentSelected.persona.str_prs_nombre_completo,
  );
  const periodSelected = useSelector(state => state.allData.periodSelected);
  const periods = useSelector(state => state.allData.periods);

  const [uid_est, setUid_est] = useState(students[0].persona.uid);
  const [uid_pdo, setUid_pdo] = useState(periods[0].uid);
  const isModalStudentsVisible = useSelector(
    state => state.modal.modalStudentsOpen,
  );
  const isModalPeriodsVisible = useSelector(
    state => state.modal.modalPeriodsOpen,
  );

  const changeModalStudentsVisible = bool => {
    dispatch(changeModalStudents(bool));
  };

  const changeModalPeriodsVisible = bool => {
    dispatch(changeModalPeriods(bool));
  };

  const setData = item => {
    setUid_est(item.uid);
    dispatch(selectedStudent(item));
  };

  const setDataPeriods = item => {
    setUid_pdo(item.uid);
    dispatch(selectedPeriod(item));
  };

  return (
    <View style={{backgroundColor: bgColor}} opacity={0.7}>
      <TouchableOpacity
        style={styles.touch}
        onPress={() => changeModalStudentsVisible(true)}>
        <Text style={styles.label}>Estudiante:</Text>
        <Text style={styles.select}>{studentSelected}</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.touch}
        onPress={() => changeModalPeriodsVisible(true)}>
        {type == 'two' ? <Text style={styles.label}>Periodo:</Text> : []}
        {type == 'two' ? (
          <Text style={styles.select}>{periodSelected.periodo}</Text>
        ) : (
          []
        )}
      </TouchableOpacity>

      <Modal
        transparent={true}
        animationType="fade"
        visible={isModalStudentsVisible}
        nRequestClose={() => changeModalStudentsVisible(false)}>
        <ModalPiker
          changeModalVisible={changeModalStudentsVisible}
          setData={setData}
          typeOptions={'students'}
          options={students}
        />
      </Modal>
      <Modal
        transparent={true}
        animationType="fade"
        visible={isModalPeriodsVisible}
        nRequestClose={() => changeModalPeriodsVisible(false)}>
        <ModalPiker
          changeModalVisible={changeModalPeriodsVisible}
          setData={setDataPeriods}
          typeOptions={'periods'}
          options={periods}
        />
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  label: {
    color: colors.white,
    fontSize: 20,
    paddingHorizontal: 10,
  },
  select: {
    color: colors.black,
    fontSize: 18,
    paddingHorizontal: 15,
  },
});
export default FilterHeader;
