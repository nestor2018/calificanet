import React from 'react';
import {Text} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {useDispatch, useSelector} from 'react-redux';
import {
  createDrawerNavigator,
  DrawerItem,
  DrawerItemList,
  DrawerContentScrollView,
} from '@react-navigation/drawer';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {StyleSheet} from 'react-native';

import LoginScreen from '../screens/login/LoginScreen';
import HomeStack from '../screens/home/HomeStack';
import AbsenceStack from '../screens/absence/AbsenceStack';
import ChoresStack from '../screens/chores/ChoresStack';
import SchoolLunchStack from '../screens/schoolLunch/SchoolLunchStack';
import TreasuryStack from '../screens/treasury/TreasuryStack';
import EventsStack from '../screens/events/EventsStack';
import NewsStack from '../screens/news/NewsStack';
import SchoolsStack from '../screens/schools/SchoolsStack';
import NotesStack from '../screens/notes/NotesStack';
import MessageStack from '../screens/messages/MessageStack';
import colors from '../constants/colors';
import {logOut} from '../store/user';
import HeaderDrawer from '../components/HeaderDrawer';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const StackVavigation = () => {
  const dispatch = useDispatch();

  const close = () => {
    dispatch(logOut());
  };
  const token = useSelector(state => state.user.token);

  const CustomDrawerContent = props => {
    return (
      <DrawerContentScrollView {...props}>
        <DrawerItem
          label="Colegio UTF8"
          icon={() => <HeaderDrawer />}
          onPress={() => close()}
          style={styles.container}
        />
        <DrawerItemList {...props} />
        <DrawerItem
          label="Cerrar sesión"
          icon={() => <FontAwesome5 name={'power-off'} color="red" size={22} />}
          onPress={() => close()}
        />
      </DrawerContentScrollView>
    );
  };
  return (
    <>
      {token == null ? (
        <Stack.Navigator>
          <Stack.Screen
            name="Login"
            component={LoginScreen}
            options={{
              headerShown: false,
            }}
          />
        </Stack.Navigator>
      ) : (
        <>
          <Drawer.Navigator
            initialRouteName="Home"
            drawerContentOptions={{
              activeTintColor: `${colors.notes}`,
            }}
            drawerContent={props => <CustomDrawerContent {...props} />}>
            <Drawer.Screen
              name="Home"
              component={HomeStack}
              options={{
                drawerIcon: ({tintColor}) => (
                  <FontAwesome5 name={'home'} size={22} color={colors.blue} />
                ),
                title: 'Inicio',
              }}
            />
            <Drawer.Screen
              name="Notes"
              component={NotesStack}
              options={{
                title: 'Notas',
                drawerIcon: ({tintColor}) => (
                  <FontAwesome5
                    name={'file-alt'}
                    size={22}
                    color={colors.notes}
                  />
                ),
              }}
            />
            <Drawer.Screen
              name="Mensajeria"
              component={MessageStack}
              options={{
                title: 'Mensajería',
                drawerIcon: ({tintColor}) => (
                  <FontAwesome5
                    name={'envelope'}
                    size={22}
                    color={colors.messagge}
                  />
                ),
                headerStyle: {
                  backgroundColor: colors.white,
                  shadowColor: colors.white,
                },
              }}
            />
            <Drawer.Screen
              name="Tareas"
              component={ChoresStack}
              options={{
                title: 'Tareas',
                drawerIcon: ({tintColor}) => (
                  <FontAwesome5
                    name={'list-ol'}
                    size={22}
                    color={colors.chore}
                  />
                ),
                headerStyle: {
                  backgroundColor: colors.white,
                  shadowColor: colors.white,
                },
              }}
            />
            <Drawer.Screen
              name="Restaurante"
              component={SchoolLunchStack}
              options={{
                title: 'Almuerzo Escolar',
                drawerIcon: ({tintColor}) => (
                  <FontAwesome5
                    name={'utensils'}
                    size={22}
                    color={colors.schoolLunch}
                  />
                ),
                headerStyle: {
                  backgroundColor: colors.white,
                  shadowColor: colors.white,
                },
              }}
            />
            <Drawer.Screen
              name="Inasistencias"
              component={AbsenceStack}
              options={{
                title: 'Inasistencias',
                drawerIcon: ({tintColor}) => (
                  <FontAwesome5
                    name={'users'}
                    size={22}
                    color={colors.absence}
                  />
                ),
                headerStyle: {
                  backgroundColor: colors.white,
                  shadowColor: colors.white,
                },
              }}
            />
            <Drawer.Screen
              name="Tesoreria"
              component={TreasuryStack}
              options={{
                title: 'Tesorería',
                drawerIcon: ({tintColor}) => (
                  <FontAwesome5 name={'dollar-sign'} size={22} />
                ),
                headerStyle: {
                  backgroundColor: colors.white,
                  shadowColor: colors.white,
                },
              }}
            />
            <Drawer.Screen
              name="Eventos"
              component={EventsStack}
              options={{
                title: 'Eventos',
                drawerIcon: ({tintColor}) => (
                  <FontAwesome5
                    name={'calendar-alt'}
                    size={22}
                    color={colors.events}
                  />
                ),
                headerStyle: {
                  backgroundColor: colors.white,
                  shadowColor: colors.white,
                },
              }}
            />
            <Drawer.Screen
              name="Noticias"
              component={NewsStack}
              options={{
                title: 'Noticias',
                drawerIcon: ({tintColor}) => (
                  <FontAwesome5
                    name={'newspaper'}
                    size={22}
                    color={colors.news}
                  />
                ),
                headerStyle: {
                  backgroundColor: colors.white,
                  shadowColor: colors.white,
                },
              }}
            />
            <Drawer.Screen
              name="Colegios"
              component={SchoolsStack}
              options={{
                title: 'Colegios',
                drawerIcon: ({tintColor}) => (
                  <FontAwesome5 name={'school'} size={22} />
                ),
                headerStyle: {
                  backgroundColor: colors.white,
                  shadowColor: colors.white,
                },
              }}
            />
          </Drawer.Navigator>
        </>
      )}
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.blue,
    padding: -20,
    margin: -20,
  },
});

export default StackVavigation;
