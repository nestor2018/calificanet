import {configureStore, combineReducers} from '@reduxjs/toolkit';
import {persistStore, persistReducer} from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';

import userSlice from './user';
import colegiosSlice from './colegios';
import modalSlice from './modal';
import allDataSlice from './allData';
import colegioSelectedSlice from './colegioSelected';
import notesSlice from './notes';
import newsSlice from './news';
import choresSlice from './chores';
import eventsSlice from './events';
import schoolLunchSlice from './schoolLunch';
import absenceSlice from './absence';
import messaggesSlice from './messagges';
import filesSlice from './files';
import newMessaggeSlice from './newMessage';
import boletinSlice from './boletin';

const reducer = combineReducers({
  user: userSlice,
  colegiosLogin: colegiosSlice,
  colegioSelected: colegioSelectedSlice,
  modal: modalSlice,
  notes: notesSlice,
  allData: allDataSlice,
  news: newsSlice,
  chores: choresSlice,
  events: eventsSlice,
  schoolLunch: schoolLunchSlice,
  absence: absenceSlice,
  messagges: messaggesSlice,
  files: filesSlice,
  newMessagge: newMessaggeSlice,
  boletin: boletinSlice,
});

const persistConfig = {
  key: 'root16',
  version: 1,
  storage: AsyncStorage,
  //almacena las propiedades de state que queremos guardar
  whitelist: [
    'user',
    'allData',
    'colegioSelected',
    'colegiosLogin',
    'newMessagge',
  ],
  //almacena las propiedades del estate que no queremos guardar
  //blacklist: [],
};

const persistedReducer = persistReducer(persistConfig, reducer);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});

export const persistor = persistStore(store);
