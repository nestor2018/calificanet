import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';
import {Toast} from '@kyupss/native-popup';

export const notes = createAsyncThunk('notes/notes', async info => {
  let formdata = new FormData();
  formdata.append(
    'data',
    `{"token" : "${info.token}", "uid_est" : "${info.uid_est}", "uid_pdo": "${info.uid_pdo}"}`,
  );
  let response = await fetch(`${info.url}notas`, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
    },
    method: 'POST',
    body: formdata,
  })
    .then(response => response.json())
    .then(data => {
      return data;
    })
    .catch(err => console.log(err, 'error notas'));
  return response;
});

let notesSlice = createSlice({
  name: 'notes',
  initialState: {
    notes: [],
    noteSelected: {},
    status: '',
  },
  reducers: {
    selectedNote: (state, action) => {
      state.noteSelected = action.payload;
    },
  },
  extraReducers: {
    [notes.pending]: state => {
      state.status = 'loading';
    },
    [notes.fulfilled]: (state, action) => {
      if (action.payload.message == 'No session found.') {
        state.status = 'success';
        Toast.show({
          title: 'Inicie sesión',
          text: 'Debido a que ingresó desde otro dispositivo debe cerrar sesión e ingresar de nuevo',
          color: 'red',
          position: 'top', // bottom, center, top
        });
      } else {
        state.notes = action.payload.data.materias;
        state.status = 'success';
      }
    },
    [notes.rejected]: state => {
      state.status = 'failed';
    },
  },
});

export const {selectedNote} = notesSlice.actions;

export default notesSlice.reducer;
