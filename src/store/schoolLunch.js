import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';
import {Toast} from '@kyupss/native-popup';

export const schoolLunch = createAsyncThunk(
  'schoolLunch/schoolLunch',
  async info => {
    let formdata = new FormData();
    formdata.append('data', `{"token" : "${info.token}"}`);
    let response = await fetch(`${info.url}restaurante`, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      method: 'POST',
      body: formdata,
    })
      .then(response => response.json())
      .then(data => {
        return data;
      })
      .catch(err => console.log(err, 'error notas'));
    return response;
  },
);

let schoolLunchSlice = createSlice({
  name: 'schoolLunch',
  initialState: {
    schoolLunch: [],
    status: '',
  },
  reducers: {},
  extraReducers: {
    [schoolLunch.pending]: state => {
      state.status = 'loading';
    },
    [schoolLunch.fulfilled]: (state, action) => {
      if (action.payload.message == 'No session found.') {
        state.status = 'success';
        Toast.show({
          title: 'Inicie sesión',
          text: 'Debido a que ingresó desde otro dispositivo debe cerrar sesión e ingresar de nuevo',
          color: 'red',
          position: 'top', // bottom, center, top
        });
      } else {
        state.schoolLunch = action.payload.menu;
        state.status = 'success';
      }
    },
    [schoolLunch.rejected]: state => {
      state.status = 'failed';
    },
  },
});

export const {} = schoolLunchSlice.actions;

export default schoolLunchSlice.reducer;
