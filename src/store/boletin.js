import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';
import {Toast} from '@kyupss/native-popup';

export const boletin = createAsyncThunk('boletin/boletin', async info => {
  console.log(info, 'estra boletin');
  let formdata = new FormData();
  formdata.append(
    'data',
    `{"token" : "${info.token}", "uid_est" : "${info.uid_est}", "uid_pdo": "${info.uid_pdo}", "uid_per": "${info.uid_per}"}`,
  );
  let response = await fetch(`${info.url}boletin`, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
    },
    method: 'POST',
    body: formdata,
  })
    .then(response => response.json())
    .then(data => {
      return data;
    })
    .catch(err => console.log(err, 'error notas'));
  return response;
});

let boletinSlice = createSlice({
  name: 'notes',
  initialState: {
    boletin: [],
    status: '',
  },
  reducers: {},
  extraReducers: {
    [boletin.pending]: state => {
      state.status = 'loading';
    },
    [boletin.fulfilled]: (state, action) => {
      if (action.payload.message == 'No session found.') {
        state.status = 'success';
        Toast.show({
          title: 'Inicie sesión',
          text: 'Debido a que ingresó desde otro dispositivo debe cerrar sesión e ingresar de nuevo',
          color: 'red',
          position: 'top', // bottom, center, top
        });
      } else {
        console.log(action.payload, '-----');
        state.boletin = action.payload.boletin;
        state.status = 'success';
      }
    },
    [boletin.rejected]: state => {
      state.status = 'failed';
    },
  },
});

export const {} = boletinSlice.actions;

export default boletinSlice.reducer;
