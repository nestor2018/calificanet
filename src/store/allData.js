import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';
import {Toast} from '@kyupss/native-popup';

export const allData = createAsyncThunk('allData/allData', async info => {
  console.log(info.token, 'info');
  let formdata = new FormData();
  formdata.append(
    'data',
    `{"token" : "${info.token}", "uid_per" : "${info.uid_per}"}`,
  );
  let response = await fetch(`${info.url}allData`, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
    },
    method: 'POST',
    body: formdata,
  })
    .then(response => response.json())
    .then(data => {
      return data;
    })
    .catch(err => console.log(err, 'error de buzon'));
  return response;
});

export const received = createAsyncThunk('received/allData', async info => {
  let formdata = new FormData();
  formdata.append('data', `{"token" : "${info.token}"}`);
  let response = await fetch(`${info.url}buzon`, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
    },
    method: 'POST',
    body: formdata,
  })
    .then(response => response.json())
    .then(data => {
      return data;
    })
    .catch(err => console.log(err, 'error'));
  return response;
});

let colegiosSlice = createSlice({
  name: 'allData',
  initialState: {
    allData: [],
    notes: [],
    sendMessages: [],
    receivedMessages: [],
    paperMessages: [],
    messgeRecipients: [],
    restaurantMenu: [],
    periods: [],
    periodSelected: null,
    news: [],
    status: '',
    statusReceived: '',
    typeGrid: 'receivedMessages',
    messageSelected: null,
  },
  reducers: {
    changeTypeGrid: (state, action) => {
      state.typeGrid = action.payload;
    },
    selectedPeriod: (state, action) => {
      state.periodSelected = action.payload;
    },
    selectedMessage: (state, action) => {
      state.messageSelected = action.payload;
    },
  },
  extraReducers: {
    [allData.pending]: (state, action) => {
      state.status = 'loading';
    },
    [allData.fulfilled]: (state, action) => {
      if (action.payload.message == 'No session found.') {
        state.status = 'success';
        Toast.show({
          title: 'Inicie sesión',
          text: 'Debido a que ingresó desde otro dispositivo debe cerrar sesión e ingresar de nuevo',
          color: 'red',
          position: 'top', // bottom, center, top
        });
      } else {
        console.log('alldata');
        state.status = 'success';
        state.allData = action.payload;
        state.messgeRecipients = action.payload.destinatario;
        state.periods = action.payload.periodos;
        state.periodSelected = action.payload.periodos[0];
      }
    },
    [allData.rejected]: (state, action) => {
      state.status = 'failed';
    },
    [received.pending]: (state, action) => {
      state.statusReceived = 'loading';
    },
    [received.fulfilled]: (state, action) => {
      if (action.payload.message == 'No session found.') {
        state.statusReceived = 'success';
        Toast.show({
          title: 'Inicie sesión',
          text: 'Debido a que ingresó desde otro dispositivo debe cerrar sesión e ingresar de nuevo',
          color: 'red',
          position: 'top', // bottom, center, top
        });
      } else {
        state.statusReceived = 'success';
        state.receivedMessages = action.payload.mensajes;
      }
    },
    [received.rejected]: (state, action) => {
      state.statusReceived = 'failed';
    },
  },
});

export const {changeTypeGrid, selectedMessage, selectedPeriod} =
  colegiosSlice.actions;

export default colegiosSlice.reducer;
