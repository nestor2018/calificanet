import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';
import {Toast} from '@kyupss/native-popup';

export const messagges = createAsyncThunk('messagges/messages', async info => {
  let formdata = new FormData();
  formdata.append('data', `{"token" : "${info.token}"}`);
  let response = await fetch(`${info.url}buzon`, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
    },
    method: 'POST',
    body: formdata,
  })
    .then(response => response.json())
    .then(data => {
      return data;
    })
    .catch(err => console.log(err, 'error notas'));
  return response;
});

export const mensajesEnviados = createAsyncThunk(
  'mensajesEnviados/messages',
  async info => {
    let formdata = new FormData();
    formdata.append('data', `{"token" : "${info.token}"}`);
    let response = await fetch(`${info.url}enviados`, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      method: 'POST',
      body: formdata,
    })
      .then(response => response.json())
      .then(data => {
        return data;
      })
      .catch(err => console.log(err, 'error notas'));
    return response;
  },
);

export const deleteMessagges = createAsyncThunk(
  'deleteMessagges/messages',
  async info => {
    let formdata = new FormData();
    formdata.append(
      'data',
      `{"token" : "${info.token}", "uids" : ["${info.uids}"]}`,
    );
    let response = await fetch(`${info.url}eliminar_mensaje`, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      method: 'POST',
      body: formdata,
    })
      .then(response => response.json())
      .then(data => {
        return data;
      })
      .catch(err => console.log(err, 'error notas'));
    return response;
  },
);

export const restaurarMessagges = createAsyncThunk(
  'restaurarMessagges/messages',
  async info => {
    console.log(info, 'info restaurar');
    let formdata = new FormData();
    formdata.append(
      'data',
      `{"token" : "${info.token}", "uids" : ["${info.uids}"]}`,
    );
    console.log(formdata, 'formdata restaurar');
    let response = await fetch(`${info.url}recuperar_papelera`, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      method: 'POST',
      body: formdata,
    })
      .then(response => response.json())
      .then(data => {
        console.log(data, 'restaurarMessagges');
        return data;
      })
      .catch(err => console.log(err, 'error notas'));
    return response;
  },
);

export const eliminarPapeleraStore = createAsyncThunk(
  'eliminarPapeleraStore/messages',
  async info => {
    console.log(info, 'info restaurar');
    let formdata = new FormData();
    formdata.append(
      'data',
      `{"token" : "${info.token}", "uids" : ["${info.uids}"]}`,
    );
    console.log(formdata, 'formdata restaurar');
    let response = await fetch(`${info.url}eliminar_papelera`, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      method: 'POST',
      body: formdata,
    })
      .then(response => response.json())
      .then(data => {
        return data;
      })
      .catch(err => console.log(err, 'error notas'));
    return response;
  },
);

export const papeleraMesage = createAsyncThunk(
  'papeleraMesage/messagges',
  async info => {
    let formdata = new FormData();
    formdata.append('data', `{"token" : "${info.token}"}`);
    let response = await fetch(`${info.url}papelera`, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      method: 'POST',
      body: formdata,
    })
      .then(response => response.json())
      .then(data => {
        return data;
      })
      .catch(err => console.log(err, 'error notas'));
    return response;
  },
);

let messaggesSlice = createSlice({
  name: 'messagges',
  initialState: {
    enviados: [],
    buzon: [],
    papelera: [],
    status: '',
    statusBuzon: '',
  },
  reducers: {},
  extraReducers: {
    [messagges.pending]: state => {
      state.status = 'loading';
    },
    [messagges.fulfilled]: (state, action) => {
      if (action.payload.message == 'No session found.') {
        state.status = 'success';
        Toast.show({
          title: 'Inicie sesión',
          text: 'Debido a que ingresó desde otro dispositivo debe cerrar sesión e ingresar de nuevo',
          color: 'red',
          position: 'top', // bottom, center, top
        });
      } else {
        state.buzon = action.payload.mensajes;
        state.status = 'success';
      }
    },
    [messagges.rejected]: state => {
      state.status = 'failed';
    },
    [papeleraMesage.pending]: state => {
      state.status = 'loading';
    },
    [papeleraMesage.fulfilled]: (state, action) => {
      console.log(action.payload, 'papelera');
      if (action.payload.message == 'No session found.') {
        state.status = 'success';
        Toast.show({
          title: 'Inicie sesión',
          text: 'Debido a que ingresó desde otro dispositivo debe cerrar sesión e ingresar de nuevo',
          color: 'red',
          position: 'top', // bottom, center, top
        });
      } else {
        state.papelera = action.payload.mensajes;
        state.status = 'success';
      }
    },
    [papeleraMesage.rejected]: state => {
      state.status = 'failed';
    },
    [mensajesEnviados.pending]: state => {
      state.status = 'loading';
    },
    [mensajesEnviados.fulfilled]: (state, action) => {
      console.log(action.payload, 'papelera enviados');
      state.enviados = action.payload.mensajes;
      state.status = 'success';
    },
    [mensajesEnviados.rejected]: state => {
      state.status = 'failed';
    },
  },
});

export const {} = messaggesSlice.actions;

export default messaggesSlice.reducer;
