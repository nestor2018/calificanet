import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';

let modalSlice = createSlice({
  name: 'modal',
  initialState: {
    modalColegiosOpen: false,
    modalPerfilesOpen: false,
    modalStudentsOpen: false,
    modalPeriodsOpen: false,
    modalPerfilOpen: false,
    modalDestinatarioOpen: false,
  },
  reducers: {
    changeModalColegios: (state, action) => {
      state.modalColegiosOpen = action.payload;
    },
    changeModalPerfiles: (state, action) => {
      state.modalPerfilesOpen = action.payload;
    },
    changeModalStudents: (state, action) => {
      state.modalStudentsOpen = action.payload;
    },
    changeModalPeriods: (state, action) => {
      state.modalPeriodsOpen = action.payload;
    },
    changeModalPerfil: (state, action) => {
      state.modalPerfilOpen = action.payload;
    },
    changeModalDestinatario: (state, action) => {
      state.modalDestinatarioOpen = action.payload;
    },
  },
});

export const {
  changeModalColegios,
  changeModalPerfiles,
  changeModalStudents,
  changeModalPeriods,
  changeModalPerfil,
  changeModalDestinatario,
} = modalSlice.actions;

export default modalSlice.reducer;
