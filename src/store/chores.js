import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';
import {Toast} from '@kyupss/native-popup';

export const chores = createAsyncThunk('chores/chores', async info => {
  let formdata = new FormData();
  formdata.append(
    'data',
    `{"token" : "${info.token}", "uid_est" : "${info.uid_est}"}`,
  );
  let response = await fetch(`${info.url}tareas_estudiante`, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
    },
    method: 'POST',
    body: formdata,
  })
    .then(response => response.json())
    .then(data => {
      return data;
    })
    .catch(err => console.log(err, 'error notas'));
  return response;
});

let choresSlice = createSlice({
  name: 'chores',
  initialState: {
    chores: [],
    chore: {},
    status: '',
  },
  reducers: {
    selectedChore: (state, action) => {
      state.chore = action.payload;
    },
  },
  extraReducers: {
    [chores.pending]: state => {
      state.status = 'loading';
    },
    [chores.fulfilled]: (state, action) => {
      if (action.payload.message == 'No session found.') {
        state.status = 'success';
        Toast.show({
          title: 'Inicie sesión',
          text: 'Debido a que ingresó desde otro dispositivo debe cerrar sesión e ingresar de nuevo',
          color: 'red',
          position: 'top', // bottom, center, top
        });
      } else {
        state.chores = action.payload.tareas;
        state.status = 'success';
      }
    },
    [chores.rejected]: state => {
      state.status = 'failed';
    },
  },
});

export const {selectedChore} = choresSlice.actions;

export default choresSlice.reducer;
