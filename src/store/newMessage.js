import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';
import {Toast} from '@kyupss/native-popup';

export const newMessagge = createAsyncThunk(
  'newMessagge/newMessagge',
  async info => {
    let formdata = new FormData();
    formdata.append('data', `{"token" : "${info.token}"}`);
    let response = await fetch(`${info.url}destinatarios`, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      method: 'POST',
      body: formdata,
    })
      .then(response => response.json())
      .then(data => {
        return data;
      })
      .catch(err => console.log(err, 'error notas'));
    return response;
  },
);

export const sendMessage = createAsyncThunk(
  'sendMessage/newMessagge',
  async info => {
    console.log(info, 'info');
    let formdata = new FormData();
    formdata.append(
      'data',
      `{"token" : "${info.token}", "asunto": "${info.asunto}", "mensaje": "${info.mensaje}", "destinatarios": "[${info.destinatarios}]"}`,
    );
    console.log(formdata, 'formdata');
    let response = await fetch(`${info.url}enviar_mensaje`, {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'multipart/form-data',
      },
      method: 'POST',
      body: formdata,
    })
      .then(response => response.json())
      .then(data => {
        return data;
      })
      .catch(err => console.log(err, 'error enviar'));
    return response;
  },
);

let newMessaggeSlice = createSlice({
  name: 'newMessagge',
  initialState: {
    allDestinatarios: [],
    status: '',
    destinatariosNombre: [],
    destinatariosUsuario: [],
    asunto: '',
    mensaje: '',
    index: '',
  },
  reducers: {
    obtenerIndex: (state, action) => {
      state.index = action.payload;
    },
    addDestinatariosNombre: (state, action) => {
      const includeNombre = state.destinatariosNombre.includes(action.payload);
      if (includeNombre) {
      } else {
        state.destinatariosNombre.push(action.payload);
      }
    },
    addDestinatariosUsuario: (state, action) => {
      const includeUsuario = state.destinatariosUsuario.includes(
        action.payload,
      );
      if (includeUsuario) {
      } else {
        state.destinatariosUsuario.push(action.payload);
      }
    },
    deleteDestinatarios: (state, action) => {
      state.destinatariosNombre.splice(state.index, 1);
      state.destinatariosUsuario.splice(state.index, 1);
    },
    addAsunto: (state, action) => {
      state.asunto = action.payload;
    },
    addMensaje: (state, action) => {
      state.mensaje = action.payload;
    },
    deleteArray: (state, action) => {
      state.destinatariosUsuario.splice(0, state.destinatariosUsuario.length);
      state.destinatariosNombre.splice(0, state.destinatariosNombre.length);
    },
  },
  extraReducers: {
    [newMessagge.pending]: state => {
      state.status = 'loading';
    },
    [newMessagge.fulfilled]: (state, action) => {
      state.allDestinatarios = action.payload.mensajes;
      state.status = 'success';
    },
    [newMessagge.rejected]: state => {
      state.status = 'failed';
    },
    [sendMessage.pending]: state => {
      state.status = 'loading';
    },
    [sendMessage.fulfilled]: (state, action) => {
      state.status = 'success';
      if (action.payload.estado == true) {
        Toast.show({
          title: 'Mensaje enviado correctamente',
          text: '',
          color: 'green',
          position: 'top', // bottom, center, top
        });
      } else {
        Toast.show({
          title: 'Error al enviar el mensaje',
          text: 'Intentelo nuevamente',
          color: 'red',
          position: 'top', // bottom, center, top
        });
      }
    },
    [sendMessage.rejected]: state => {
      state.status = 'failed';
    },
  },
});

export const {
  obtenerIndex,
  addDestinatariosNombre,
  addDestinatariosUsuario,
  deleteDestinatarios,
  addAsunto,
  addMensaje,
  deleteArray,
} = newMessaggeSlice.actions;

export default newMessaggeSlice.reducer;
