import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';
import {useDispatch, useSelector} from 'react-redux';
import md5 from 'md5';
import {Toast} from '@kyupss/native-popup';

import apiConfig from '../config/api';

export const signIn = createAsyncThunk('user/signIn', async ({credentials}) => {
  console.log(credentials, 'credentials');
  let password = md5(credentials.clave);
  let formdata = new FormData();
  formdata.append(
    'data',
    `{"usuario" : "${credentials.usuario}", "clave": "${password}", "perfil": "${credentials.perfil}", "tipoDevice": "${credentials.tipoDevice}", "tokenDevice": "${credentials.tokenDevice}"}`,
  );
  console.log(formdata, 'formdata');
  let options = {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
      'Content-Type': 'multipart/form-data',
    },
    body: formdata,
  };
  let response = await fetch(`${credentials.url}loginApp`, options)
    .then(response => response.json())
    .then(data => {
      console.log(data, 'data');
      return data;
    })
    .catch(error => console.error(error, 'el error del login'));
  return response;
});

let userSlice = createSlice({
  name: 'user',
  initialState: {
    noSesion: false,
    user: [],
    token: null,
    students: [],
    status: '',
    studentSelected: null,
    nameUser: '',
    tokens: [],
    tokenDevice: null,
    nameColegion: [],
  },
  reducers: {
    logOut: state => {
      state.token = null;
    },
    selectedStudent: (state, action) => {
      state.studentSelected = action.payload;
    },
    noSesionUser: (state, action) => {
      state.noSesion = action.payload;
    },
    saveTokenDevice: (state, action) => {
      console.log(action.payload);
      state.tokenDevice = action.payload;
    },
  },
  extraReducers: {
    [signIn.pending]: state => {
      state.status = 'loading';
    },
    [signIn.fulfilled]: (state, action) => {
      if (action.payload.mensaje == 'Perfil no Asociado') {
        Toast.show({
          title: 'Error de credenciales',
          text: 'Verifique sus credenciales y vuelva a intetar.',
          color: 'red',
          position: 'top', // bottom, center, top
        });
        state.status = 'success';
      } else {
        state.studentSelected =
          action.payload.info_usuario.familiar.estudiantes[0];
        state.user = action.payload;
        state.nameUser = action.payload.info_usuario.str_usu_nombre;
        state.token = action.payload.token;
        state.students = action.payload.info_usuario.familiar.estudiantes;
        state.status = 'success';
        state.tokens.push(action.payload.token);
      }
    },
    [signIn.rejected]: state => {
      state.status = 'failed';
    },
  },
});

export const {logOut, selectedStudent, noSesionUser, saveTokenDevice} =
  userSlice.actions;

export default userSlice.reducer;
