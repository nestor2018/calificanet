import {createSlice} from '@reduxjs/toolkit';

let colegioSelectedSlice = createSlice({
  name: 'colegioSelected',
  initialState: {
    url: '',
    urlImages: '',
  },
  reducers: {
    saveUrl: (state, action) => {
      state.url = action.payload;
    },
    saveUrlImage: (state, action) => {
      state.urlImages = action.payload;
    },
  },
});

export const {saveUrl, saveUrlImage} = colegioSelectedSlice.actions;

export default colegioSelectedSlice.reducer;
