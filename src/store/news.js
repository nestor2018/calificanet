import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';
import {Toast} from '@kyupss/native-popup';

export const news = createAsyncThunk('news/news', async info => {
  let formdata = new FormData();
  formdata.append(
    'data',
    `{"token" : "${info.token}", "uid_per" : "${info.uid_per}"}`,
  );
  let response = await fetch(`${info.url}noticias`, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
    },
    method: 'POST',
    body: formdata,
  })
    .then(response => response.json())
    .then(data => {
      return data;
    })
    .catch(err => console.log(err, 'error notas'));
  return response;
});

let newsSlice = createSlice({
  name: 'news',
  initialState: {
    news: [],
    newSelected: {},
    status: '',
  },
  reducers: {
    selectedNew: (state, action) => {
      state.newSelected = action.payload;
    },
  },
  extraReducers: {
    [news.pending]: state => {
      state.status = 'loading';
    },
    [news.fulfilled]: (state, action) => {
      if (action.payload.message == 'No session found.') {
        state.status = 'success';
        Toast.show({
          title: 'Inicie sesión',
          text: 'Debido a que ingresó desde otro dispositivo debe cerrar sesión e ingresar de nuevo',
          color: 'red',
          position: 'top', // bottom, center, top
        });
      } else {
        state.news = action.payload.noticias;
        state.status = 'success';
      }
    },
    [news.rejected]: state => {
      state.status = 'failed';
    },
  },
});

export const {selectedNew} = newsSlice.actions;

export default newsSlice.reducer;
