import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';

export const absence = createAsyncThunk('absence/absence', async info => {
  let formdata = new FormData();
  formdata.append(
    'data',
    `{"token" : "${info.token}", "uid_est" : "${info.uid_est}", "uid_pdo" : "${info.uid_pdo}"}`,
  );
  let response = await fetch(`${info.url}inasistencias`, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
    },
    method: 'POST',
    body: formdata,
  })
    .then(response => response.json())
    .then(data => {
      return data;
    })
    .catch(err => console.log(err, 'error notas'));
  return response;
});

let absenceSlice = createSlice({
  name: 'absence',
  initialState: {
    noSesion: false,
    absence: [],
    absenceSelected: {},
    status: '',
  },
  reducers: {
    selectedAbsence: (state, action) => {
      state.absenceSelected = action.payload;
    },
  },
  extraReducers: {
    [absence.pending]: state => {
      state.status = 'loading';
    },
    [absence.fulfilled]: (state, action) => {
      if (action.payload.message == 'No session found.') {
        state.noSesion = true;
        state.status = 'success';
      } else {
        state.absence = action.payload.inasistencias;
        state.status = 'success';
        state.noSesion = false;
      }
    },
    [absence.rejected]: state => {
      state.status = 'failed';
    },
  },
});

export const {selectedAbsence} = absenceSlice.actions;

export default absenceSlice.reducer;
