import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';

import apiConfig from '../config/api';

export const colegios = createAsyncThunk('colegios/colegios', async () => {
  let formdata = new FormData();
  formdata.append('data', `{"token" : "a9125dc05cb197dd3197d735f9f83ce3"}`);
  let response = await fetch(apiConfig.domainColegios, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
    },
    method: 'POST',
    body: formdata,
  })
    .then(response => response.json())
    .then(data => {
      return data;
    })
    .catch(err => {
      console.log(err, 'error');
    });
  return response.colegios;
});

let colegiosSlice = createSlice({
  name: 'colegios',
  initialState: {
    dataColegios: [],
    status: '',
  },
  reducers: {},
  extraReducers: {
    [colegios.pending]: (state, action) => {
      state.status = 'loading';
    },
    [colegios.fulfilled]: (state, action) => {
      state.dataColegios = action.payload;
      state.status = 'success';
    },
    [colegios.rejected]: (state, action) => {
      state.status = 'failed';
    },
  },
});

export const {} = colegiosSlice.actions;

export default colegiosSlice.reducer;
