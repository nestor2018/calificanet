import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';

export const file = createAsyncThunk('file/file', async info => {
  let formdata = new FormData();
  formdata.append(
    'data',
    `{"token" : "${info.token}", "uid_est" : "${info.uid_est}", "view" : "0", "save" : "1", "path": ""}`,
  );
  let response = await fetch(`${info.url}trazabilidad`, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
    },
    method: 'POST',
    body: formdata,
  })
    .then(response => response.json())
    .then(data => {
      return data;
    })
    .catch(err => console.log(err, 'error notas'));
  return response;
});

let filesSlice = createSlice({
  name: 'file',
  initialState: {
    url: '',
    title: '',
    archivoTesoreria: '',
  },
  reducers: {
    changeUrl: (state, action) => {
      state.url = action.payload;
    },
    changeTitle: (state, action) => {
      state.title = action.payload;
    },
  },
  extraReducers: {
    [file.pending]: state => {
      state.status = 'loading';
    },
    [file.fulfilled]: (state, action) => {
      if (action.payload.message == 'No session found.') {
        state.status = 'success';
        Toast.show({
          title: 'Inicie sesión',
          text: 'Debido a que ingresó desde otro dispositivo debe cerrar sesión e ingresar de nuevo',
          color: 'red',
          position: 'top', // bottom, center, top
        });
      } else {
        state.archivoTesoreria = action.payload.url;
        state.status = 'success';
      }
    },
    [file.rejected]: state => {
      state.status = 'failed';
    },
  },
});

export const {changeUrl, changeTitle} = filesSlice.actions;

export default filesSlice.reducer;
