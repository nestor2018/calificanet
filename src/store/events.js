import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';
import {Toast} from '@kyupss/native-popup';

export const events = createAsyncThunk('events/events', async info => {
  let formdata = new FormData();
  formdata.append(
    'data',
    `{"token" : "${info.token}", "uid_per" : "${info.uid_per}"}`,
  );
  let response = await fetch(`${info.url}eventos`, {
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
    },
    method: 'POST',
    body: formdata,
  })
    .then(response => response.json())
    .then(data => {
      return data;
    })
    .catch(err => console.log(err, 'error notas'));
  return response;
});

let eventsSlice = createSlice({
  name: 'events',
  initialState: {
    events: [],
    status: '',
  },
  reducers: {},
  extraReducers: {
    [events.pending]: state => {
      state.status = 'loading';
    },
    [events.fulfilled]: (state, action) => {
      if (action.payload.message == 'No session found.') {
        state.status = 'success';
        Toast.show({
          title: 'Inicie sesión',
          text: 'Debido a que ingresó desde otro dispositivo debe cerrar sesión e ingresar de nuevo',
          color: 'red',
          position: 'top', // bottom, center, top
        });
      } else {
        state.events = action.payload.eventos;
        state.status = 'success';
      }
    },
    [events.rejected]: state => {
      state.status = 'failed';
    },
  },
});

export const {} = eventsSlice.actions;

export default eventsSlice.reducer;
