const colors = {
  white: '#ffffff',
  black: '#000000',
  error: '#F29091',
  blue: '#543CFF',
  notes: '#1380A8',
  filterNotes: '#1380A8',
  messagge: '#0B3B5C',
  chore: '#1C5C44',
  schoolLunch: '#DB6518',
  absence: '#9029A8',
  events: '#C27817',
  news: '#CF200C',
  darkBlue: '#000CFA',
};

export default colors;
