import React, {useEffect} from 'react';
import {
  ScrollView,
  View,
  Text,
  SafeAreaView,
  ActivityIndicator,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {Root} from '@kyupss/native-popup';

import {allData} from '../../store/allData';
import {selectedStudent} from '../../store/user';
import colors from '../../constants/colors';

const Home = ({navigation}) => {
  const url = useSelector(state => state.colegioSelected.url);
  const token = useSelector(state => state.user.token);
  const uid_per = useSelector(state => state.user.user.info_usuario.perfil.uid);
  const student = useSelector(state => state.user.students[0]);
  const statusAllData = useSelector(state => state.allData.status);
  const dispatch = useDispatch();
  const data = useSelector(state => state.files);
  console.log(token);

  useEffect(() => {
    allDataa();
  }, []);

  const allDataa = () => {
    dispatch(selectedStudent(student));
    dispatch(
      allData({
        token,
        url,
        uid_per,
      }),
    );
  };

  const goScreen = (root, screen) => {
    navigation.navigate(root, {
      screen,
    });
  };

  return (
    <Root>
      <SafeAreaView>
        <ScrollView>
          {statusAllData == 'loading' ? (
            <View style={styles.containerLoading}>
              <ActivityIndicator size="large" color={colors.blue} />
            </View>
          ) : null}
          <View style={styles.container}>
            <TouchableOpacity
              style={[styles.box, styles.bgNotes]}
              onPress={() => goScreen('Notes', 'notas')}>
              <FontAwesome5 name={'file-alt'} size={30} color={colors.white} />
              <Text style={styles.text}>Notas</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.box, styles.bgmessagge]}
              onPress={() => goScreen('Mensajeria', 'mensajes')}>
              <FontAwesome5 name={'envelope'} size={30} color={colors.white} />
              <Text style={styles.text}>Mensajeía</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.box, styles.bgChore]}
              onPress={() => goScreen('Tareas', 'Tareas')}>
              <FontAwesome5 name={'list-ol'} size={30} color={colors.white} />
              <Text style={styles.text}>Tareas</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.box, styles.bgSchoolLunch]}
              onPress={() => goScreen('Restaurante', 'Restaurante')}>
              <FontAwesome5 name={'utensils'} size={30} color={colors.white} />
              <Text style={styles.text}>Almuerzos</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.box, styles.bgAbsence]}
              onPress={() => goScreen('Inasistencias', 'Inasistencias')}>
              <FontAwesome5 name={'users'} size={30} color={colors.white} />
              <Text style={styles.text}>Asistencia</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.box, styles.bgNotes]}
              onPress={() => goScreen('Tesoreria', 'Tesoreria')}>
              <FontAwesome5
                name={'dollar-sign'}
                size={30}
                color={colors.white}
              />
              <Text style={styles.text}>Tesorería</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.box, styles.bgEvents]}
              onPress={() => goScreen('Eventos', 'Eventos')}>
              <FontAwesome5
                name={'calendar-alt'}
                size={30}
                color={colors.white}
              />
              <Text style={styles.text}>Eventos</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.box, styles.bgNews]}
              onPress={() => goScreen('Noticias', 'Noticias')}>
              <FontAwesome5 name={'newspaper'} size={30} color={colors.white} />
              <Text style={styles.text}>Noticias</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={[styles.box, styles.bgNews]}
              onPress={() => goScreen('Colegios', 'Colegios')}>
              <FontAwesome5 name={'school'} size={30} color={colors.white} />
              <Text style={styles.text}>Colegios</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </SafeAreaView>
    </Root>
  );
};

const styles = StyleSheet.create({
  containerLoading: {
    marginTop: 50,
  },
  container: {
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    color: colors.white,
  },
  box: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 130,
    height: 130,
    margin: 20,
  },
  text: {
    color: colors.white,
    marginTop: 30,
  },
  bgChore: {
    backgroundColor: colors.chore,
  },
  bgNotes: {
    backgroundColor: colors.notes,
  },
  bgmessagge: {
    backgroundColor: colors.messagge,
  },
  bgSchoolLunch: {
    backgroundColor: colors.schoolLunch,
  },
  bgAbsence: {
    backgroundColor: colors.absence,
  },
  bgEvents: {
    backgroundColor: colors.events,
  },
  bgNews: {
    backgroundColor: colors.news,
  },
});
export default Home;
