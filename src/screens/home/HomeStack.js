import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import HomeScreen from './HomeScreen';
import colors from '../../constants/colors';

const HomeStack = ({navigation}) => {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Inicio"
        component={HomeScreen}
        options={{
          title: 'INICIO',
          headerStyle: {backgroundColor: colors.blue},
          headerLeftContainerStyle: {paddingLeft: 20},
          headerTintColor: `${colors.white}`,
          headerLeft: () => (
            <FontAwesome5
              name={'bars'}
              color={colors.white}
              size={22}
              onPress={() => navigation.openDrawer()}
            />
          ),
        }}
      />
    </Stack.Navigator>
  );
};

export default HomeStack;
