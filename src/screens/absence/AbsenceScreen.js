import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {Root, Popup} from '@kyupss/native-popup';

import {logOut, noSesionUser} from '../../store/user';
import FilterHeader from '../../components/FilterHeader';
import {absence, selectedAbsence} from '../../store/absence';
import ItemSeparator from '../../components/ItemSeparator';
import colors from '../../constants/colors';

const Item = ({item, onPress, backgroundColor, textColor, navigation}) => {
  return (
    <TouchableOpacity onPress={onPress} style={[styles.item, backgroundColor]}>
      <View style={styles.container}>
        <View style={styles.flex}>
          <Text style={styles.item}>Materia: </Text>
          <Text>{item.str_mat_nombre}</Text>
        </View>
        <View style={styles.flex}>
          <Text style={styles.item}>Fecha: </Text>
          <Text>{item.fec_ina_fecha}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const Absence = ({navigation}) => {
  const dispatch = useDispatch();
  const token = useSelector(state => state.user.token);
  const periods = useSelector(state => state.allData.periods);
  const studentSelected = useSelector(state => state.user.studentSelected);
  const periodSelected = useSelector(state => state.allData.periodSelected);
  const url = useSelector(state => state.colegioSelected.url);
  const noSesion = useSelector(state => state.absence.noSesion);
  const absenceSelected = useSelector(state => state.absence.absence);
  const statusAbsence = useSelector(state => state.absence.status);
  const [uid_pdo, setUid_pdo] = useState(periods[0].uid);
  const [student, setStudent] = useState(studentSelected);
  let getAbsence = () => {
    dispatch(
      absence({
        url,
        token,
        uid_est: studentSelected.uid,
        uid_pdo,
      }),
    );
  };

  const absenceSelecte = item => {
    console.log(item);
    dispatch(selectedAbsence(item));
    navigation.navigate('DetailAbsence');
  };

  const renderItem = ({item}) => (
    <Item item={item} onPress={() => absenceSelecte(item)} />
  );

  useEffect(() => {
    getAbsence();
    if (noSesion) {
      console.log('entra aca');
      dispatch(logOut());
      dispatch(noSesionUser(true));
      navigation.navigate('Login');
    }
  }, []);

  useEffect(() => {
    getAbsence();
    if (noSesion) {
      console.log('entra aca2');
      dispatch(logOut());
      dispatch(noSesionUser(true));
      navigation.navigate('Login');
    }
  }, [studentSelected]);

  useEffect(() => {
    getAbsence();
    if (noSesion) {
      console.log('entra aca3');
      dispatch(logOut());
      dispatch(noSesionUser(true));
      navigation.navigate('Login');
    }
  }, [periodSelected]);

  return (
    <Root>
      <SafeAreaView>
        <FilterHeader type="two" bgColor={colors.absence} />
        {statusAbsence == 'loading' ? (
          <View style={styles.containerLoading}>
            <ActivityIndicator size="large" color={colors.absence} />
          </View>
        ) : null}
        <FlatList
          data={absenceSelected}
          renderItem={renderItem}
          keyExtractor={item => item.uid}
          ListEmptyComponent={<Text>No hay inasistencias</Text>}
          ItemSeparatorComponent={({highlighted}) => <ItemSeparator />}
          extraData={student}
          refreshing={true}
        />
      </SafeAreaView>
    </Root>
  );
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  flex: {
    flexDirection: 'row',
    padding: 10,
  },
  item: {
    fontWeight: 'bold',
  },
  containerLoading: {
    marginTop: 50,
  },
});

export default Absence;
