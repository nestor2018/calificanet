import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {useDispatch, useSelector} from 'react-redux';

import AbsenceScreen from './AbsenceScreen';
import LoginScreen from '../login/LoginScreen';
import DetailAbsence from './DetailAbsence';
import colors from '../../constants/colors';

const AbsenceStack = ({navigation}) => {
  const Stack = createStackNavigator();
  const absence = useSelector(state => state.absence.absenceSelected);
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Inasistencias"
        component={AbsenceScreen}
        options={{
          title: 'INASISTENCIAS',
          headerStyle: {backgroundColor: colors.absence},
          headerTintColor: `${colors.white}`,
          headerLeftContainerStyle: {paddingLeft: 20},
          headerLeft: () => (
            <FontAwesome5
              name={'bars'}
              color={colors.white}
              size={22}
              onPress={() => navigation.openDrawer()}
            />
          ),
        }}
      />
      <Stack.Screen
        name="DetailAbsence"
        component={DetailAbsence}
        options={{
          headerStyle: {backgroundColor: colors.absence},
          title: `Inasistencia`,
          headerTintColor: `${colors.white}`,
          headerBackTitleVisible: false,
        }}
      />
      <Stack.Screen
        name="Login"
        component={LoginScreen}
        options={{
          title: '',
          headerStyle: {
            backgroundColor: colors.white,
            shadowColor: colors.white,
          },
        }}
      />
    </Stack.Navigator>
  );
};

export default AbsenceStack;
