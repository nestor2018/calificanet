import React from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import colors from '../../constants/colors';

const DetailAbsence = () => {
  const absence = useSelector(state => state.absence.absenceSelected);
  return (
    <ScrollView style={styles.container}>
      <View style={styles.flex}>
        <Text style={styles.aspecto}>Materia: </Text>
        <Text style={styles.text}>{absence.str_mat_nombre}</Text>
      </View>
      <View style={styles.flex}>
        <Text style={styles.aspecto}>Fecha: </Text>
        <Text style={styles.text}>{absence.fec_ina_fecha}</Text>
      </View>
      <View style={styles.flex}>
        <Text style={styles.aspecto}>Horas: </Text>
        <Text style={styles.text}>{absence.num_ina_cantidad_horas}</Text>
      </View>
      <View style={styles.flex}>
        <Text style={styles.aspecto}>Motivo: </Text>
        <Text style={styles.text}>
          {absence.motivo_inasistencia.str_min_descripcion}
        </Text>
      </View>
      <View style={styles.flex}>
        <Text style={styles.aspecto}>Justificada: </Text>
        {absence.boo_ina_justificada == 1 ? (
          <Text style={styles.text}>Si </Text>
        ) : (
          <Text style={styles.text}>No </Text>
        )}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: 50,
  },
  flex: {
    flexDirection: 'row',
    padding: 4,
  },
  titleText: {
    fontWeight: 'bold',
    paddingHorizontal: 10,
    textAlign: 'center',
  },
  text: {
    paddingHorizontal: 15,
  },
  aspecto: {
    padding: 10,
    textAlign: 'left',
    fontWeight: 'bold',
    color: colors.black,
  },
  text: {
    padding: 10,
    textAlign: 'left',
    color: colors.black,
  },
});

export default DetailAbsence;
