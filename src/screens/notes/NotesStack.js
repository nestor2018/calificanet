import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {useDispatch, useSelector} from 'react-redux';

import NotesScreen from './NotesScreen';
import DetailNoteScreen from './DetailNoteScreen';
import colors from '../../constants/colors';

const NotesStack = ({navigation}) => {
  const Stack = createStackNavigator();
  const note = useSelector(state => state.notes.noteSelected);
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="notas"
        component={NotesScreen}
        options={{
          title: 'NOTAS',
          headerStyle: {backgroundColor: colors.notes},
          headerTintColor: `${colors.white}`,
          headerLeftContainerStyle: {paddingLeft: 20},
          headerLeft: () => (
            <FontAwesome5
              name={'bars'}
              color={colors.white}
              size={22}
              onPress={() => navigation.openDrawer()}
            />
          ),
        }}
      />
      <Stack.Screen
        name="DetailNote"
        component={DetailNoteScreen}
        options={{
          headerStyle: {backgroundColor: colors.notes},
          title: `${note.materia}`,
          headerTintColor: `${colors.white}`,
          headerBackTitleVisible: false,
        }}
      />
    </Stack.Navigator>
  );
};

export default NotesStack;
