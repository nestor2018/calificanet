import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
  Button,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import FileViewer from 'react-native-file-viewer';
import RNFS from 'react-native-fs';
import {Root} from '@kyupss/native-popup';

import FilterHeader from '../../components/FilterHeader';
import ItemSeparator from '../../components/ItemSeparator';
import {notes, selectedNote} from '../../store/notes';
import {boletin} from '../../store/boletin';
import colors from '../../constants/colors';

const Item = ({item, onPress, backgroundColor, textColor, navigation}) => {
  return (
    <TouchableOpacity onPress={onPress} style={styles.container}>
      <View style={styles.flex}>
        <Text style={styles.item}>{item.materia}</Text>
        <Text>
          {item.aspectos ? (
            <FontAwesome5
              name={'angle-right'}
              color={colors.notes}
              size={25}
              style={styles.icon}
            />
          ) : (
            []
          )}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

const Notes = ({navigation}) => {
  const dispatch = useDispatch();
  const url = useSelector(state => state.colegioSelected.url);
  const statusNotes = useSelector(state => state.notes.status);
  const notesStore = useSelector(state => state.notes.notes);
  const token = useSelector(state => state.user.token);
  const periodSelected = useSelector(state => state.allData.periodSelected);
  const studentSelected = useSelector(state => state.user.studentSelected);
  const perfil = useSelector(state => state.user.user.info_usuario.perfil.uid);
  const boletinStore = useSelector(state => state.boletin);
  const urlImages = useSelector(state => state.colegioSelected.urlImages);
  console.log(boletinStore, 'boletin');

  const getNotes = () => {
    dispatch(
      notes({
        url,
        token,
        uid_est: studentSelected.uid,
        uid_pdo: periodSelected.uid,
      }),
    );
  };

  const getboletin = () => {
    dispatch(
      boletin({
        url,
        token,
        uid_est: studentSelected.uid,
        uid_pdo: periodSelected.uid,
        uid_per: perfil,
      }),
    );
  };

  useEffect(() => {
    getNotes();
    getboletin();
  }, []);

  useEffect(() => {
    getNotes();
    getboletin();
  }, [studentSelected]);

  useEffect(() => {
    getNotes();
    getboletin();
  }, [periodSelected]);

  const noteSelected = item => {
    if (item.aspectos) {
      dispatch(selectedNote(item));
      navigation.navigate('DetailNote');
    }
  };

  const renderItem = ({item}) => (
    <Item item={item} onPress={() => noteSelected(item)} />
  );

  const verBoletin = () => {
    openFile(boletinStore.boletin[0].fil_esb_ruta);
  };

  const openFile = file => {
    console.log(file, 'file');
    const typeDocument = file.substr(-3);
    const path = `${urlImages}${file}`;
    const localFile = `${RNFS.DocumentDirectoryPath}/temporaryfile.${typeDocument}`;
    if (typeDocument == 'pdf' || typeDocument == 'jpg') {
      const options = {
        fromUrl: path,
        toFile: localFile,
      };
      RNFS.downloadFile(options)
        .promise.then(() => FileViewer.open(localFile))
        .then(() => {
          // success
        })
        .catch(error => {
          // error
        });
    } else {
      dispatch(changeUrl(path));
      dispatch(changeTitle('title'));
      navigation.navigate('Browser');
    }
  };

  return (
    <Root>
      <SafeAreaView>
        <View>
          <FilterHeader type="two" bgColor={colors.filterNotes} />
          {statusNotes == 'loading' ? (
            <View style={styles.containerLoading}>
              <ActivityIndicator size="large" color={colors.notes} />
            </View>
          ) : null}
          {boletinStore.boletin.length > 0 ? (
            <Button onPress={verBoletin} title="VER BOLETIN" />
          ) : null}
          <FlatList
            data={notesStore}
            renderItem={renderItem}
            keyExtractor={item => item.materia}
            ListEmptyComponent={<Text>No hay notas disponibles</Text>}
            ItemSeparatorComponent={({highlighted}) => <ItemSeparator />}
          />
        </View>
      </SafeAreaView>
    </Root>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
    textAlign: 'center',
  },
  flex: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
    alignItems: 'center',
  },
  item: {
    width: '85%',
  },
  icon: {
    flex: 1,
  },
  containerLoading: {
    marginTop: 50,
  },
});

export default Notes;
