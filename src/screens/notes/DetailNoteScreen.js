import React from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';

import colors from '../../constants/colors';

const DetailNoteScreen = () => {
  const note = useSelector(state => state.notes.noteSelected);
  return (
    <ScrollView style={styles.container}>
      {note.aspectos.map(aspecto => (
        <View>
          <Text style={styles.aspecto}>{aspecto.aspecto}</Text>
          {aspecto.detalles ? (
            <View style={styles.flex}>
              <Text style={styles.titleText}>Logro</Text>
              <Text style={styles.titleText}>Nota</Text>
            </View>
          ) : null}
          {aspecto.detalles
            ? aspecto.detalles.map(detalle => (
                <View style={styles.flex}>
                  <Text style={styles.text}>{detalle.logro}</Text>
                  <Text style={styles.text}>{detalle.nota}</Text>
                </View>
              ))
            : null}
        </View>
      ))}
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: 50,
  },
  flex: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 4,
  },
  titleText: {
    fontWeight: 'bold',
    paddingHorizontal: 10,
    textAlign: 'center',
  },
  text: {
    paddingHorizontal: 15,
  },
  aspecto: {
    padding: 10,
    textAlign: 'left',
    fontWeight: 'bold',
    color: colors.notes,
  },
});

export default DetailNoteScreen;
