import React from 'react';
import {View, Text, StyleSheet, ScrollView, Button, Image} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import FileViewer from 'react-native-file-viewer';
import RNFS from 'react-native-fs';

import colors from '../../constants/colors';
import {changeUrl, changeTitle} from '../../store/files';

const DetailNew = ({navigation}) => {
  const noticia = useSelector(state => state.news.newSelected);
  const dispatch = useDispatch();
  const urlImages = useSelector(state => state.colegioSelected.urlImages);
  const openFile = file => {
    const typeDocument = file.substr(-3);
    const path = `${urlImages}${file}`;
    const localFile = `${RNFS.DocumentDirectoryPath}/temporaryfile.${typeDocument}`;
    if (
      typeDocument == 'pdf' ||
      typeDocument == 'jpg' ||
      typeDocument == 'png'
    ) {
      console.log('entra al if');

      const options = {
        fromUrl: path,
        toFile: localFile,
      };
      RNFS.downloadFile(options)
        .promise.then(() => FileViewer.open(localFile))
        .then(() => {
          // success
        })
        .catch(error => {
          // error
        });
    } else {
      dispatch(changeUrl(path));
      dispatch(changeTitle('title'));
      navigation.navigate('Browser');
    }
  };
  return (
    <ScrollView style={styles.container}>
      <View style={styles.flex}>
        <Text style={styles.aspecto}>{noticia.str_nti_titulo}</Text>
      </View>
      <View style={styles.flex}>
        <Text style={styles.text}>{noticia.mem_nti_texto}</Text>
      </View>
      {noticia.fil_nti_foto != '' ? (
        <Image
          style={styles.tinyLogo}
          source={{
            uri: `${urlImages}${noticia.fil_nti_foto}`,
          }}
        />
      ) : null}
      <View style={styles.button}>
        {noticia.fil_nti_foto != '' ? (
          <Button
            onPress={() => openFile(noticia.fil_nti_foto)}
            title="VER IMAGEN"
            color={colors.news}
            accessibilityLabel="Learn more about this purple button"
          />
        ) : null}
        {noticia.fil_nti_anexo != '' ? (
          <Button
            onPress={() => openFile(noticia.fil_nti_anexo)}
            title="VER ANEXO"
            color={colors.news}
            accessibilityLabel="Learn more about this purple button"
          />
        ) : null}
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginBottom: 50,
  },
  flex: {
    flexDirection: 'row',
    padding: 4,
  },
  titleText: {
    fontWeight: 'bold',
    paddingHorizontal: 10,
    textAlign: 'center',
  },
  text: {
    paddingHorizontal: 15,
  },
  aspecto: {
    padding: 10,
    textAlign: 'left',
    fontWeight: 'bold',
    color: colors.black,
  },
  text: {
    padding: 10,
    textAlign: 'left',
    color: colors.black,
  },
  button: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  tinyLogo: {
    textAlign: 'center',
    alignSelf: 'center',
    width: 60,
    height: 60,
  },
});

export default DetailNew;
