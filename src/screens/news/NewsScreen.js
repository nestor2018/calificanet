import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  SafeAreaView,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {Root} from '@kyupss/native-popup';

import {news, selectedNew} from '../../store/news';
import ItemSeparator from '../../components/ItemSeparator';
import colors from '../../constants/colors';

const Item = ({item, onPress, backgroundColor, textColor, navigation}) => {
  const urlImages = useSelector(state => state.colegioSelected.urlImages);
  return (
    <TouchableOpacity onPress={onPress} style={[styles.item, backgroundColor]}>
      <Text style={styles.item}>{item.str_nti_titulo}</Text>
    </TouchableOpacity>
  );
};

const News = ({navigation}) => {
  const dispatch = useDispatch();
  const url = useSelector(state => state.colegioSelected.url);
  const token = useSelector(state => state.user.token);
  const uid_per = useSelector(state => state.user.user.info_usuario.perfil.uid);
  const newsSelected = useSelector(state => state.news.news);
  const statusNews = useSelector(state => state.news.status);

  let getNews = () => {
    dispatch(
      news({
        token,
        url,
        uid_per,
      }),
    );
  };
  useEffect(() => {
    getNews();
  }, []);
  const newSelected = item => {
    console.log(item);
    dispatch(selectedNew(item));
    navigation.navigate('DetailNew');
  };
  const renderItem = ({item}) => (
    <Item item={item} onPress={() => newSelected(item)} />
  );
  return (
    <Root>
      <SafeAreaView>
        <View>
          {statusNews == 'loading' ? (
            <View style={styles.containerLoading}>
              <ActivityIndicator size="large" color={colors.news} />
            </View>
          ) : null}
          <FlatList
            data={newsSelected}
            renderItem={renderItem}
            keyExtractor={item => item.uid}
            ListEmptyComponent={<Text>No hay noticias disponibles</Text>}
            ItemSeparatorComponent={({highlighted}) => <ItemSeparator />}
          />
        </View>
      </SafeAreaView>
    </Root>
  );
};

const styles = StyleSheet.create({
  item: {
    padding: 10,
    fontWeight: 'bold',
  },
  image: {
    width: 200,
    height: 200,
  },
  description: {
    paddingTop: 8,
  },
  containerLoading: {
    marginTop: 50,
  },
});

export default News;
