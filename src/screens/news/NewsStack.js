import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import NewsScreen from './NewsScreen';
import DetailNew from './DetailNew';
import colors from '../../constants/colors';
import BrowserScreen from '../browser/Browser';

const NewsStack = ({navigation}) => {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Noticias"
        component={NewsScreen}
        options={{
          title: 'NOTICIAS',
          headerStyle: {backgroundColor: colors.news},
          headerTintColor: `${colors.white}`,
          headerLeftContainerStyle: {paddingLeft: 20},
          headerLeft: () => (
            <FontAwesome5
              name={'bars'}
              color={colors.white}
              size={22}
              onPress={() => navigation.openDrawer()}
            />
          ),
        }}
      />
      <Stack.Screen
        name="DetailNew"
        component={DetailNew}
        options={{
          headerStyle: {backgroundColor: colors.news},
          title: `Noticia`,
          headerTintColor: `${colors.white}`,
          headerBackTitleVisible: false,
        }}
      />
      <Stack.Screen
        name="Browser"
        component={BrowserScreen}
        options={{
          headerTintColor: `${colors.white}`,
          title: ``,
          headerBackTitleVisible: false,
          handlerLeft: false,
        }}
      />
    </Stack.Navigator>
  );
};

export default NewsStack;
