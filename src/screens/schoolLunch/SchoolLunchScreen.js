import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  Button,
  ActivityIndicator,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import FileViewer from 'react-native-file-viewer';
import RNFS from 'react-native-fs';
import {Root} from '@kyupss/native-popup';

import {schoolLunch} from '../../store/schoolLunch';
import ItemSeparator from '../../components/ItemSeparator';
import colors from '../../constants/colors';
import {changeUrl, changeTitle} from '../../store/files';

const Item = ({item, onPress, backgroundColor, textColor, navigation}) => {
  console.log(item, 'item');
  const dispatch = useDispatch();
  const urlImages = useSelector(state => state.colegioSelected.urlImages);

  const openFile = file => {
    const typeDocument = file.substr(-3);
    if (typeDocument == 'pdf' || typeDocument == 'jpg') {
      console.log('entra al if');
      const path = `${urlImages}${file}`;
      const localFile = `${RNFS.DocumentDirectoryPath}/temporaryfile.${typeDocument}`;

      const options = {
        fromUrl: path,
        toFile: localFile,
      };
      RNFS.downloadFile(options)
        .promise.then(() => FileViewer.open(localFile))
        .then(() => {
          // success
        })
        .catch(error => {
          // error
        });
    } else {
      dispatch(changeUrl(path));
      dispatch(changeTitle('title'));
      navigation.navigate('Browser');
    }
  };

  return (
    <TouchableOpacity onPress={onPress} style={[styles.item, backgroundColor]}>
      <Text style={styles.item}>{item.str_rtt_titulo}</Text>
      <Button
        onPress={() => openFile(item.fil_rtt_archivo)}
        title="VER"
        color={colors.schoolLunch}
        accessibilityLabel="Learn more about this purple button"
      />
    </TouchableOpacity>
  );
};

const SchoolLunch = ({navigation}) => {
  const dispatch = useDispatch();
  const token = useSelector(state => state.user.token);
  const statusSchoolLunch = useSelector(state => state.schoolLunch.status);
  const menu = useSelector(state => state.schoolLunch.schoolLunch);
  const url = useSelector(state => state.colegioSelected.url);
  let getSchoolLunch = () => {
    dispatch(
      schoolLunch({
        url,
        token,
      }),
    );
  };
  useEffect(() => {
    getSchoolLunch();
  }, []);

  const menuSelected = item => {
    console.log(item, 'chore');
    //dispatch(selectedNote(item));
    //navigation.navigate('DetailNote');
  };

  const renderItem = ({item}) => (
    <Item
      item={item}
      navigation={navigation}
      onPress={() => menuSelected(item)}
    />
  );

  return (
    <Root>
      <SafeAreaView>
        <View>
          {statusSchoolLunch == 'loading' ? (
            <View style={styles.containerLoading}>
              <ActivityIndicator size="large" color={colors.schoolLunch} />
            </View>
          ) : null}
          <FlatList
            data={menu}
            renderItem={renderItem}
            keyExtractor={item => item.uid}
            ListEmptyComponent={<Text>No hay menus disponibles</Text>}
            ItemSeparatorComponent={({highlighted}) => <ItemSeparator />}
          />
        </View>
      </SafeAreaView>
    </Root>
  );
};

const styles = StyleSheet.create({
  item: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },
  containerLoading: {
    marginTop: 50,
  },
});

export default SchoolLunch;
