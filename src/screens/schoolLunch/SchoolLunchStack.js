import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import SchoolLunchScreen from './SchoolLunchScreen';
import colors from '../../constants/colors';
import BrowserScreen from '../browser/Browser';

const SchoolLunchStack = ({navigation}) => {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Restaurante"
        component={SchoolLunchScreen}
        options={{
          title: 'RESTAURANTE',
          headerStyle: {backgroundColor: colors.schoolLunch},
          headerTintColor: `${colors.white}`,
          headerLeftContainerStyle: {paddingLeft: 20},
          headerLeft: () => (
            <FontAwesome5
              name={'bars'}
              color={colors.white}
              size={22}
              onPress={() => navigation.openDrawer()}
            />
          ),
        }}
      />
      <Stack.Screen
        name="Browser"
        component={BrowserScreen}
        options={{
          headerStyle: {backgroundColor: colors.schoolLunch},
          headerTintColor: `${colors.white}`,
          title: `titulo`,
          headerBackTitleVisible: false,
        }}
      />
    </Stack.Navigator>
  );
};

export default SchoolLunchStack;
