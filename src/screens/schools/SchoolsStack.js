import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import SchoolsScreen from './SchoolsScreen';
import LoginScreen from '../login/LoginScreen';
import colors from '../../constants/colors';

const SchoolsStack = ({navigation}) => {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Colegios"
        component={SchoolsScreen}
        options={{
          title: 'COLEGIOS',
          headerStyle: {backgroundColor: colors.blue},
          headerLeftContainerStyle: {paddingLeft: 20},
          headerLeft: () => (
            <FontAwesome5
              name={'bars'}
              size={22}
              onPress={() => navigation.openDrawer()}
            />
          ),
        }}
      />
      <Stack.Screen
        name="Login"
        component={LoginScreen}
        options={{
          title: '',
          headerStyle: {
            backgroundColor: colors.white,
            shadowColor: colors.white,
          },
        }}
      />
    </Stack.Navigator>
  );
};

export default SchoolsStack;
