import React from 'react';
import {View, Text, SafeAreaView, StyleSheet} from 'react-native';
import {Root} from '@kyupss/native-popup';

const SchoolsScreen = ({navigation}) => {
  const newLogin = () => {
    navigation.navigate('DetailAbsence');
  };
  return (
    <Root>
      <SafeAreaView>
        <View style={styles.container}>
          <Text style={styles.text}>Colegios 2</Text>
        </View>
      </SafeAreaView>
    </Root>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'red',
  },
  text: {
    color: 'blue',
  },
});

export default SchoolsScreen;
