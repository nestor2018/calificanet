import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';

import LoginScreen from './LoginScreen';
import colors from '../../constants/colors';

const Stack = createStackNavigator();

const LoginStack = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Login"
        component={LoginScreen}
        options={{
          title: '',
          headerStyle: {
            backgroundColor: colors.white,
            shadowColor: colors.white,
          },
        }}
      />
    </Stack.Navigator>
  );
};

export default LoginStack;
