import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  Button,
  Modal,
  Image,
  Dimensions,
  Platform,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {Root, Toast, Popup} from '@kyupss/native-popup';
import messaging from '@react-native-firebase/messaging';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import {signIn, noSesionUser, saveTokenDevice} from '../../store/user';
import {colegios} from '../../store/colegios';
import colors from '../../constants/colors';
import ModalPiker from '../../components/ModalPiker';
import {changeModalColegios, changeModalPerfiles} from '../../store/modal';
import {saveUrl, saveUrlImage} from '../../store/colegioSelected';

const Login = props => {
  const [tipeDevice, setTipeDevice] = useState('device');
  const [user, setUser] = useState('');
  const [password, setPassword] = useState('');
  const [focusInputUser, setFocusInputUser] = useState(false);
  const [focusInputPassword, setFocusInputPassword] = useState(false);
  const [errorInputUser, setErrorInputUser] = useState(false);
  const [errorInputPassword, setErrorInputPassword] = useState(false);
  const [chooseColegio, setChooseColegio] = useState('Selecciona un colegio');
  const [choosePerfiles, setChoosePerfiles] = useState('Seleccione un perfil');
  const [perfiles, setPerfiles] = useState([]);
  const [perfileUser, setPerfilUser] = useState([]);
  const colegiosInfo = useSelector(state => state.colegiosLogin.dataColegios);
  const noSesionLogin = useSelector(state => state.user.noSesion);
  const isModalColegiosVisible = useSelector(
    state => state.modal.modalColegiosOpen,
  );
  const tokenDeviceSave = useSelector(state => state.user.tokenDevice);
  const url = useSelector(state => state.colegioSelected.url);
  const isModalPerfilesVisible = useSelector(
    state => state.modal.modalPerfilesOpen,
  );

  const changeModalColegiosVisible = bool => {
    dispatch(changeModalColegios(bool));
  };

  const changeModalPerfilesVisible = bool => {
    dispatch(changeModalPerfiles(bool));
  };

  const dispatch = useDispatch();
  let doSignIn = () => {
    dispatch(noSesionUser(false));

    if (user.length < 1) {
      setErrorInputUser(true);
      Toast.show({
        title: 'Campo incompleto',
        text: 'Debe digitar un usuario',
        color: 'red',
        position: 'top', // bottom, center, top
      });
    } else if (password.length < 1) {
      setErrorInputUser(true);
      Toast.show({
        title: 'Campo incompleto',
        text: 'Debe digitar una contraseña valida',
        color: 'red',
        position: 'top', // bottom, center, top
      });
    } else {
      dispatch(
        signIn({
          credentials: {
            //usuario: user,
            //clave: password,
            //perfil: perfileUser,
            url,
            usuario: user,
            clave: password,
            perfil: perfileUser,
            tipoDevice: tipeDevice,
            tokenDevice: tokenDeviceSave,
          },
        }),
      );
    }
  };

  const setData = item => {
    setPerfiles(item.perfiles);
    setChooseColegio(item.str_col_nombre);
    dispatch(saveUrl(item.str_col_url + item.str_col_ruta_api));
    dispatch(saveUrlImage(item.str_col_url));
  };

  const setDataPerfiles = item => {
    setChoosePerfiles(item.str_cpe_perfil);
    setPerfilUser(item.str_cpe_uid_per);
  };

  useEffect(() => {
    const saveToken = messaging()
      .getToken()
      .then(token => {
        console.log(token, 'token device');
        dispatch(saveTokenDevice(token));
      });
    dispatch(colegios());
    if (noSesionLogin) {
      Popup.show({
        type: 'Warning',
        title: 'Inicie sesión',
        button: false,
        textBody:
          'Debido a que ingresó desde otro dispositivo se ha cerrado la sesión por favor ingrese de nuevo',
        buttonText: 'Ok',
        callback: () => Popup.hide(),
      });
    }
    if (Platform.OS === 'ios') {
      setTipeDevice('ios');
    } else {
      setTipeDevice('android');
    }
    return () => {
      saveToken();
    };
  }, []);

  const focusUser = () => {
    setErrorInputUser(false);
    setFocusInputUser(true);
  };
  const pressUser = () => {
    setErrorInputUser(false);
    setFocusInputUser(true);
  };
  const endEditUser = () => {
    if (user.length < 1) {
      setErrorInputUser(true);
      setFocusInputUser(false);
    } else {
      setErrorInputUser(false);
      setFocusInputUser(false);
    }
  };
  const focusPassword = () => {
    setErrorInputPassword(false);
    setFocusInputPassword(true);
  };
  const pressPassword = () => {
    setErrorInputPassword(false);
    setFocusInputPassword(true);
  };
  const endEditPassword = () => {
    setFocusInputPassword(false);
  };

  return (
    <Root>
      <View
        style={[
          styles.container,
          isModalColegiosVisible && styles.opacity,
          isModalPerfilesVisible && styles.opacity,
        ]}>
        <View style={styles.containerImg}>
          <Image
            source={require('../../images/screen.png')}
            style={styles.image}
          />
        </View>
        <TextInput
          style={[
            styles.input,
            focusInputUser && styles.inputFocus,
            errorInputUser && styles.inputError,
          ]}
          onChangeText={setUser}
          value={user}
          onFocus={focusUser}
          onKeyPress={pressUser}
          onEndEditing={endEditUser}
          placeholder="Usuario"
        />
        <TextInput
          style={[
            styles.input,
            focusInputPassword && styles.inputFocus,
            errorInputPassword && styles.inputError,
          ]}
          onChangeText={setPassword}
          onFocus={focusPassword}
          onKeyPress={pressPassword}
          onEndEditing={endEditPassword}
          value={password}
          secureTextEntry={true}
          placeholder="Contraseña"
        />
        <TouchableOpacity
          style={styles.buttonModal}
          onPress={() => changeModalColegiosVisible(true)}>
          <Text style={styles.textInput}>{chooseColegio}</Text>
          <FontAwesome5 name={'angle-down'} color={colors.black} size={22} />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.buttonModal}
          onPress={() => changeModalPerfilesVisible(true)}>
          <Text style={styles.textInput}>{choosePerfiles}</Text>
          <FontAwesome5 name={'angle-down'} color={colors.black} size={22} />
        </TouchableOpacity>
        <Modal
          transparent={true}
          animationType="fade"
          visible={isModalColegiosVisible}
          nRequestClose={() => changeModalColegiosVisible(false)}>
          <ModalPiker
            changeModalVisible={changeModalColegiosVisible}
            setData={setData}
            typeOptions={'colegios'}
            options={colegiosInfo}
          />
        </Modal>
        <Modal
          transparent={true}
          animationType="fade"
          visible={isModalPerfilesVisible}
          nRequestClose={() => changeModalPerfilesVisible(false)}>
          <ModalPiker
            changeModalVisible={changeModalPerfilesVisible}
            setData={setDataPerfiles}
            typeOptions={'perfiles'}
            options={perfiles}
          />
        </Modal>
        <Button onPress={doSignIn} title="INGRESAR" />
      </View>
    </Root>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    paddingTop: 100,
  },
  opacity: {
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  input: {
    height: 40,
    margin: 12,
    borderBottomWidth: 1,
    borderBottomColor: 'black',
    margin: 20,
  },
  inputFocus: {
    borderBottomColor: colors.blue,
    borderBottomWidth: 2,
  },
  inputError: {
    borderBottomColor: colors.error,
    borderBottomWidth: 2,
  },
  textInput: {
    fontSize: 18,
    color: colors.black,
  },
  buttonModal: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
  containerImg: {
    marginLeft: '35%',
    width: 120,
    height: 120,
  },
  image: {
    width: '100%',
    height: '100%',
  },
});

export default Login;
