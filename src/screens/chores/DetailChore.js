import React, {useState, useEffect} from 'react';
import {View, FlatList, Button, Image, Text, StyleSheet} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import FileViewer from 'react-native-file-viewer';
import RNFS from 'react-native-fs';
import DocumentPicker from 'react-native-document-picker';
import {changeUrl, changeTitle} from '../../store/files';

import colors from '../../constants/colors';

const Item = ({item, onPress, backgroundColor, textColor, navigation}) => {
  console.log(item);
  return (
    <View style={[styles.containerBox, backgroundColor]}>
      <Text style={styles.name}>- {item.mem_trr_texto}</Text>
    </View>
  );
};

const DetailChore = ({navigation}) => {
  const dispatch = useDispatch();
  const chore = useSelector(state => state.chores.chore);
  const urlImages = useSelector(state => state.colegioSelected.urlImages);
  const [visible, setVisible] = useState(false);
  console.log(chore, 'chores images');
  const onPressLearnMore = () => {
    console.log('aa');
    console.log(`${urlImages}${chore.fil_tre_anexo}`);
  };

  const openFile = file => {
    const typeDocument = file.substr(-3);
    const path = `${urlImages}${file}`;
    const localFile = `${RNFS.DocumentDirectoryPath}/temporaryfile.${typeDocument}`;
    if (typeDocument == 'pdf' || typeDocument == 'jpg') {
      const options = {
        fromUrl: path,
        toFile: localFile,
      };
      RNFS.downloadFile(options)
        .promise.then(() => FileViewer.open(localFile))
        .then(() => {
          // success
        })
        .catch(error => {
          // error
        });
    } else {
      dispatch(changeUrl(path));
      dispatch(changeTitle('title'));
      navigation.navigate('Browser');
    }
  };

  const renderItem = ({item}) => {
    return (
      <View>
        <Text style={styles.titleResponse}>Respuestas</Text>
        <Item item={item} />
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <View style={styles.containerMateria}>
        <Text style={styles.titleMateria}>Materia</Text>
        <Text style={styles.materia}>{chore.materia.str_mat_nombre}</Text>
      </View>
      <View style={styles.containerFecha}>
        <View style={styles.fecha}>
          <Text style={styles.fechaTitle}>Fecha</Text>
          <Text style={styles.fechaText}>{chore.fec_tre_fecha}</Text>
        </View>
        <View style={styles.fecha}>
          <Text style={styles.fechaTitle}>Fecha de entrega</Text>
          <Text style={styles.fechaText}>{chore.fec_tre_fecha_entrega}</Text>
        </View>
      </View>
      <Text style={styles.description}>{chore.mem_tre_texto}</Text>
      {chore.respuestas != [] ? (
        <FlatList
          data={chore.respuestas}
          renderItem={renderItem}
          keyExtractor={item => item.uid}
          ItemSeparatorComponent={({highlighted}) => <ItemSeparator />}
        />
      ) : null}
      <View style={styles.button}>
        {chore.fil_tre_anexo ? (
          <Button
            onPress={() => openFile(chore.fil_tre_anexo)}
            title="ANEXO 1"
            color={colors.chore}
            accessibilityLabel="Learn more about this purple button"
          />
        ) : null}
        {chore.fil_tre_anexo2 ? (
          <Button
            onPress={() => openFile(chore.fil_tre_anexo2)}
            title="ANEXO 2"
            color={colors.chore}
            accessibilityLabel="Learn more about this purple button"
          />
        ) : null}
        {chore.fil_tre_anexo3 ? (
          <Button
            onPress={() => openFile(chore.fil_tre_anexo3)}
            title="ANEXO 3"
            color={colors.chore}
            accessibilityLabel="Learn more about this purple button"
          />
        ) : null}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    marginHorizontal: 16,
  },
  description: {
    alignSelf: 'center',
    fontSize: 15,
    marginTop: 10,
  },
  containerFecha: {
    marginTop: 15,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  fecha: {
    padding: 5,
  },
  fechaTitle: {
    textAlign: 'center',
    fontWeight: 'bold',
  },
  fechaText: {
    textAlign: 'center',
    marginTop: 5,
  },
  containerMateria: {
    marginTop: 15,
    alignSelf: 'center',
  },
  titleMateria: {
    fontWeight: 'bold',
  },
  materia: {
    marginTop: 5,
  },
  tinyLogo: {
    alignSelf: 'center',
    width: 100,
    height: 100,
  },
  button: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  titleResponse: {
    marginTop: 20,
    fontWeight: 'bold',
  },
});

export default DetailChore;
