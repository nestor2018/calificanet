import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {useDispatch, useSelector} from 'react-redux';

import ChoresScreen from './ChoresScreen';
import DetailChore from './DetailChore';
import BrowserScreen from '../browser/Browser';
import colors from '../../constants/colors';

const ChoresStack = ({navigation}) => {
  const Stack = createStackNavigator();
  const chore = useSelector(state => state.chores.chore);
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Tareas"
        component={ChoresScreen}
        options={{
          title: 'TAREAS',
          headerStyle: {backgroundColor: colors.chore},
          headerTintColor: `${colors.white}`,
          headerLeftContainerStyle: {paddingLeft: 20},
          headerLeft: () => (
            <FontAwesome5
              name={'bars'}
              color={colors.white}
              size={22}
              onPress={() => navigation.openDrawer()}
            />
          ),
        }}
      />
      <Stack.Screen
        name="DetailChore"
        component={DetailChore}
        options={{
          headerStyle: {backgroundColor: colors.chore},
          headerTintColor: `${colors.white}`,
          title: `${chore.str_tre_titulo}`,
          headerBackTitleVisible: false,
        }}
      />
      <Stack.Screen
        name="Browser"
        component={BrowserScreen}
        options={{
          headerTintColor: `${colors.white}`,
          title: ``,
          headerBackTitleVisible: false,
          handlerLeft: false,
        }}
      />
    </Stack.Navigator>
  );
};

export default ChoresStack;
