import React, {useEffect, useState} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {Root} from '@kyupss/native-popup';

import {chores, selectedChore} from '../../store/chores';
import FilterHeader from '../../components/FilterHeader';
import ItemSeparator from '../../components/ItemSeparator';
import colors from '../../constants/colors';

const Item = ({item, onPress, backgroundColor, textColor, navigation}) => {
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[styles.containerBox, backgroundColor]}>
      <Text style={styles.name}>{item.str_tre_titulo}</Text>
      <Text style={styles.materia}>{item.materia.str_mat_nombre}</Text>
    </TouchableOpacity>
  );
};

const Chores = ({navigation}) => {
  const dispatch = useDispatch();
  const token = useSelector(state => state.user.token);
  const studentSelected = useSelector(state => state.user.studentSelected);
  const statusChores = useSelector(state => state.chores.status);
  const url = useSelector(state => state.colegioSelected.url);
  const choresSelected = useSelector(state => state.chores.chores);
  const [student, setStudent] = useState(studentSelected);
  let getChores = () => {
    dispatch(
      chores({
        url,
        token,
        uid_est: studentSelected.uid,
      }),
    );
  };
  useEffect(() => {
    getChores();
  }, []);
  useEffect(() => {
    getChores();
  }, [studentSelected]);
  const choreSelected = item => {
    dispatch(selectedChore(item));
    navigation.navigate('DetailChore');
  };
  const renderItem = ({item}) => (
    <Item item={item} onPress={() => choreSelected(item)} />
  );
  return (
    <Root>
      <SafeAreaView>
        <View>
          <FilterHeader type="one" bgColor={colors.chore} />
          {statusChores == 'loading' ? (
            <View style={styles.containerLoading}>
              <ActivityIndicator size="large" color={colors.chore} />
            </View>
          ) : null}
          <FlatList
            data={choresSelected}
            renderItem={renderItem}
            keyExtractor={item => item.uid}
            ListEmptyComponent={<Text>No hay tareas</Text>}
            ItemSeparatorComponent={({highlighted}) => <ItemSeparator />}
            extraData={student}
          />
        </View>
      </SafeAreaView>
    </Root>
  );
};

const styles = StyleSheet.create({
  containerBox: {
    padding: 10,
  },
  name: {
    fontSize: 15,
    marginBottom: 5,
  },
  materia: {
    color: colors.chore,
  },
  containerLoading: {
    marginTop: 50,
  },
});

export default Chores;
