import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import MessagesScreen from './MessagesScreen';
import MessageDetail from './MessageDetail';
import NewMessage from './NewMessage';
import colors from '../../constants/colors';
import BrowserScreen from '../browser/Browser';

const NotesStack = ({navigation}) => {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="mensajes"
        component={MessagesScreen}
        options={{
          title: 'MENSAJES',
          headerStyle: {backgroundColor: colors.messagge},
          headerTintColor: `${colors.white}`,
          headerLeftContainerStyle: {paddingLeft: 20},
          headerLeft: () => (
            <FontAwesome5
              name={'bars'}
              color={colors.white}
              size={22}
              onPress={() => navigation.openDrawer()}
            />
          ),
        }}
      />
      <Stack.Screen
        name="DetailMessage"
        component={MessageDetail}
        options={{
          headerStyle: {backgroundColor: colors.messagge},
          headerTintColor: `${colors.white}`,
          title: '',
          headerBackTitleVisible: false,
        }}
      />
      <Stack.Screen
        name="NewMessage"
        component={NewMessage}
        options={{
          headerStyle: {backgroundColor: colors.messagge},
          headerTintColor: `${colors.white}`,
          title: 'Nuevo Mensaje',
          headerBackTitleVisible: false,
        }}
      />
      <Stack.Screen
        name="Browser"
        component={BrowserScreen}
        options={{
          headerTintColor: `${colors.white}`,
          title: ``,
          headerBackTitleVisible: false,
          handlerLeft: false,
        }}
      />
    </Stack.Navigator>
  );
};

export default NotesStack;
