import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {Root} from '@kyupss/native-popup';
import {useDispatch, useSelector} from 'react-redux';

import TabsMessages from '../../components/TabsMessages';
import Grid from '../../components/Grid';
import colors from '../../constants/colors';
import {
  messagges,
  mensajesEnviados,
  papeleraMesage,
} from '../../store/messagges';
import {
  obtenerIndex,
  addDestinatariosUsuario,
  addDestinatariosNombre,
  newMessagge,
  addAsunto,
  addMensaje,
  deleteArray,
  deleteDestinatarios,
  sendMessage,
} from '../../store/newMessage';

const Messages = ({navigation}) => {
  const dispatch = useDispatch();
  const url = useSelector(state => state.colegioSelected.url);
  const token = useSelector(state => state.user.token);
  const statusMessage = useSelector(state => state.messagges.status);
  const uid_per = useSelector(state => state.user.user.info_usuario.perfil.uid);
  let getMesages = () => {
    dispatch(
      messagges({
        url,
        token,
        uid_per,
      }),
    );
  };

  let mensajesEnviadosStore = () => {
    dispatch(
      mensajesEnviados({
        url,
        token,
      }),
    );
  };

  const goNewMessage = () => {
    navigation.navigate('NewMessage');
    dispatch(addAsunto(''));
    dispatch(addMensaje(''));
    dispatch(deleteArray());
  };

  let getPapelera = () => {
    dispatch(
      papeleraMesage({
        url,
        token,
      }),
    );
  };
  useEffect(() => {
    getMesages();
    getPapelera();
    mensajesEnviadosStore();
  }, []);
  return (
    <Root>
      <SafeAreaView>
        <View>
          <TabsMessages />
          {statusMessage == 'loading' ? (
            <View style={styles.containerLoading}>
              <ActivityIndicator size="large" color={colors.chore} />
            </View>
          ) : null}
          <Grid navigation={navigation} />
        </View>
        <TouchableOpacity style={styles.rounder} onPress={goNewMessage}>
          <Text style={styles.title}>+</Text>
        </TouchableOpacity>
      </SafeAreaView>
    </Root>
  );
};

const styles = StyleSheet.create({
  rounder: {
    width: 70,
    height: 70,
    position: 'absolute',
    backgroundColor: colors.messagge,
    bottom: 80,
    right: 30,
    borderRadius: 70 / 2,
    justifyContent: 'center',
  },
  title: {
    textAlign: 'center',
    fontSize: 35,
    color: colors.white,
  },
  containerLoading: {
    marginTop: 50,
    marginBottom: 30,
  },
});

export default Messages;
