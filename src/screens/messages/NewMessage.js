import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  FlatList,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Modal,
  Button,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import {Root} from '@kyupss/native-popup';

import colors from '../../constants/colors';
import {
  obtenerIndex,
  addDestinatariosUsuario,
  addDestinatariosNombre,
  newMessagge,
  addAsunto,
  addMensaje,
  deleteDestinatarios,
  sendMessage,
  deleteArray,
} from '../../store/newMessage';
import {changeModalPerfil, changeModalDestinatario} from '../../store/modal';
import ModalPiker from '../../components/ModalPiker';
import {mensajesEnviados} from '../../store/messagges';

const NewMessage = () => {
  const [desti, setDesti] = useState([]);
  const dispatch = useDispatch();
  const [search, setSearch] = useState('');
  const [filteredDataSource, setFilteredDataSource] = useState([]);
  const [choosePerfil, setChoosePerfile] = useState('Todos');
  const [titleDestinatario, setTitleDestinatario] = useState(
    'Seleccione un destinatario',
  );
  const listDestinatarioNombre = useSelector(
    state => state.newMessagge.destinatariosNombre,
  );
  const listDestinatarioUsuario = useSelector(
    state => state.newMessagge.destinatariosUsuario,
  );
  let asunto = useSelector(state => state.newMessagge.asunto);
  let mensaje = useSelector(state => state.newMessagge.mensaje);
  const [asunto1, setAsunto1] = useState(asunto);
  const [mensaje1, setMensaje1] = useState(mensaje);
  const allDestinatarios = useSelector(
    state => state.newMessagge.allDestinatarios,
  );
  const destinatarios = useSelector(
    state => state.allData.allData.destinatario,
  );
  const isModalPerfilVisible = useSelector(
    state => state.modal.modalPerfilOpen,
  );
  const isModalDestinatarioVisible = useSelector(
    state => state.modal.modalDestinatarioOpen,
  );
  const changeModalPerfileVisible = bool => {
    dispatch(changeModalPerfil(bool));
  };

  const changeModalDestinatarioVisible = bool => {
    dispatch(changeModalDestinatario(bool));
  };

  const setDataPerfil = item => {
    setFiltroDestinatarios(item.destinatarios);
    setChoosePerfile(item.str_per_nombre);
  };
  const setDataDestinatario = item => {
    dispatch(addDestinatariosNombre(item.nombre));
    dispatch(addDestinatariosUsuario(item.usuario));
  };
  let items = destinatarios[8].destinatarios;
  const student = useSelector(state => state.user.students[0]);
  const [filtroDestinatarios, setFiltroDestinatarios] = useState(items);
  const token = useSelector(state => state.user.token);
  const url = useSelector(state => state.colegioSelected.url);
  //const allDestinatario = destinatarios.map(item => {
  //items = items.concat(item.destinatarios);
  //console.log(item, '------');
  //});
  let getDestinatarios = () => {
    dispatch(
      newMessagge({
        url,
        token,
      }),
    );
  };

  useEffect(() => {
    getDestinatarios();
  }, []);

  let mensajesEnviadosStore = () => {
    dispatch(
      mensajesEnviados({
        url,
        token,
      }),
    );
  };

  const enviarMessage = () => {
    dispatch(
      sendMessage({
        url,
        token,
        asunto: asunto1,
        mensaje: mensaje1,
        destinatarios: listDestinatarioUsuario,
      }),
    );
    dispatch(deleteArray());
    dispatch(addAsunto(''));
    dispatch(addMensaje(''));
    mensajesEnviadosStore();
  };

  const searchFilterFunction = text => {
    console.log(text, 'text', filteredDataSource);
    // Check if searched text is not blank
    if (text.length > 0) {
      // Inserted text is not blank
      // Filter the masterDataSource and update FilteredDataSource
      const newData = items.filter(function (item) {
        if (item != undefined) {
          // Applying filter for the inserted text in search bar
          const itemData = item.nombre
            ? item.nombre.toUpperCase()
            : ''.toUpperCase();
          const textData = text.toUpperCase();
          return itemData.indexOf(textData) > -1;
        }
      });
      console.log(newData, 'itemData');
      setFilteredDataSource(newData);
      console.log(filteredDataSource, 'data');
      setSearch(text);
    } else {
      // Inserted text is blank
      // Update FilteredDataSource with masterDataSource
      setSearch(text);
    }
  };

  const ItemView = ({item}) => {
    return (
      // Flat List Item
      <Text style={styles.itemStyle} onPress={() => getItem(item)}>
        {item.nombre.toUpperCase()}
      </Text>
    );
  };

  const ItemView2 = ({item, index}) => (
    <View style={styles.containerDestinatario}>
      <Text style={styles.destinatarioSelected}>{item}</Text>
      <TouchableOpacity onPress={() => deleteItem(item, index)}>
        <FontAwesome5 name={'times'} color={colors.white} size={18} />
      </TouchableOpacity>
    </View>
  );

  const ItemSeparatorView = () => {
    return (
      // Flat List Item Separator
      <View
        style={{
          height: 0.5,
          width: '100%',
          backgroundColor: '#C8C8C8',
        }}
      />
    );
  };

  const deleteItem = (item, index) => {
    dispatch(deleteDestinatarios(item));
    dispatch(obtenerIndex(index));
  };

  const getItem = item => {
    console.log(item);
    // Function for click on an item
    dispatch(addDestinatariosNombre(item.nombre));
    dispatch(addDestinatariosUsuario(item.usuario));
    setFilteredDataSource('');
    setSearch('');
  };

  return (
    <Root>
      <View style={styles.container}>
        <Text>Destinatarios</Text>
        <FlatList
          data={listDestinatarioNombre}
          keyExtractor={(item, index) => index.toString()}
          renderItem={ItemView2}
          horizontal={true}
        />
        <TextInput
          style={styles.textInputStyle}
          onChangeText={text => searchFilterFunction(text)}
          value={search}
          underlineColorAndroid="transparent"
          placeholder="Agrega destinarios por nombre"
        />
        {search ? (
          <FlatList
            data={filteredDataSource}
            keyExtractor={(item, index) => index.toString()}
            ItemSeparatorComponent={ItemSeparatorView}
            renderItem={ItemView}
          />
        ) : (
          <Text>nada</Text>
        )}
        <Text>Filtrar por perfil</Text>
        <TouchableOpacity
          style={styles.buttonModal}
          onPress={() => changeModalPerfileVisible(true)}>
          <Text style={styles.textInput}>{choosePerfil}</Text>
          <FontAwesome5 name={'angle-down'} color={colors.black} size={22} />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.buttonModal}
          onPress={() => changeModalDestinatarioVisible(true)}>
          <Text style={styles.textInput}>{titleDestinatario}</Text>
          <FontAwesome5 name={'angle-down'} color={colors.black} size={22} />
        </TouchableOpacity>
        <Modal
          transparent={true}
          animationType="fade"
          visible={isModalPerfilVisible}
          nRequestClose={() => changeModalPerfileVisible(false)}>
          <ModalPiker
            changeModalVisible={changeModalPerfileVisible}
            setData={setDataPerfil}
            typeOptions={'perfilMessage'}
            options={destinatarios}
          />
        </Modal>
        <Modal
          transparent={true}
          animationType="fade"
          visible={isModalDestinatarioVisible}
          nRequestClose={() => changeModalDestinatarioVisible(false)}>
          <ModalPiker
            changeModalVisible={changeModalDestinatarioVisible}
            setData={setDataDestinatario}
            typeOptions={'perfilDestinatario'}
            options={filtroDestinatarios}
          />
        </Modal>
        <TextInput
          style={styles.textInputStyle}
          onChangeText={text => setAsunto1(text)}
          value={asunto1}
          underlineColorAndroid="transparent"
          placeholder="Asunto"
        />
        <TextInput
          style={styles.textInputMessageStyle}
          onChangeText={text => setMensaje1(text)}
          value={mensaje1}
          underlineColorAndroid="transparent"
          placeholder="Mensaje"
          multiline={true}
          editable
          numberOfLines={6}
        />
        <Button
          onPress={() => enviarMessage()}
          title="Enviar"
          color={colors.messagge}
          accessibilityLabel="enviar mensaje"
        />
      </View>
    </Root>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 10,
  },
  itemStyle: {
    padding: 10,
  },
  containerDestinatario: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: colors.messagge,
    marginHorizontal: 5,
    paddingVertical: 3,
    paddingHorizontal: 8,
    borderRadius: 20,
    alignItems: 'center',
  },
  destinatarioSelected: {color: colors.white, padding: 5},
  textInputStyle: {
    height: 40,
    borderWidth: 1,
    paddingLeft: 20,
    margin: 5,
    borderColor: colors.messagge,
  },
  textInputMessageStyle: {
    borderWidth: 1,
    paddingLeft: 20,
    margin: 5,
    marginBottom: 20,
    borderColor: colors.messagge,
  },
  buttonModal: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10,
    paddingHorizontal: 20,
  },
});

export default NewMessage;
