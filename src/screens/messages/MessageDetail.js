import React from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import FileViewer from 'react-native-file-viewer';
import RNFS from 'react-native-fs';
import {changeUrl, changeTitle} from '../../store/files';

import colors from '../../constants/colors';
import {
  obtenerIndex,
  addDestinatariosUsuario,
  addDestinatariosNombre,
  newMessagge,
  addAsunto,
  addMensaje,
  deleteDestinatarios,
  sendMessage,
} from '../../store/newMessage';

const MessageDetail = ({navigation}) => {
  const dispatch = useDispatch();
  const messageSelected = useSelector(state => state.allData.messageSelected);
  const urlImages = useSelector(state => state.colegioSelected.urlImages);

  const openFile = file => {
    const typeDocument = file.substr(-3);
    const path = `${urlImages}${file}`;
    const localFile = `${RNFS.DocumentDirectoryPath}/temporaryfile.${typeDocument}`;
    if (
      typeDocument == 'pdf' ||
      typeDocument == 'jpg' ||
      typeDocument == 'png'
    ) {
      const options = {
        fromUrl: path,
        toFile: localFile,
      };
      RNFS.downloadFile(options)
        .promise.then(() => FileViewer.open(localFile))
        .then(() => {
          // success
        })
        .catch(error => {
          // error
        });
    } else {
      dispatch(changeUrl(path));
      dispatch(changeTitle('title'));
      navigation.navigate('Browser');
    }
  };

  const reenviar = () => {
    dispatch(addAsunto(messageSelected.str_bne_asunto));
    dispatch(addMensaje(messageSelected.str_bne_mensaje));
    dispatch(addDestinatariosNombre(messageSelected.remitente));
    navigation.navigate('NewMessage');
  };

  const responder = () => {
    dispatch(addAsunto(messageSelected.str_bne_asunto));
    dispatch(addMensaje(messageSelected.str_bne_mensaje));
    dispatch(addDestinatariosNombre(messageSelected.remitente));
    navigation.navigate('NewMessage');
  };

  return (
    <View>
      <Text style={styles.title}>{messageSelected.str_bne_asunto}</Text>
      <View style={styles.flex}>
        <Text style={styles.title}>De: </Text>
        {messageSelected.remitente ? (
          <Text style={styles.text}>{messageSelected.remitente}</Text>
        ) : (
          <Text style={styles.text}>{messageSelected.remitente}</Text>
        )}
      </View>
      <View style={styles.flex}>
        <Text style={styles.title}>Para: </Text>
        {messageSelected.uid_usu_destinatario ? (
          <Text style={styles.text}>
            {messageSelected.uid_usu_destinatario}
          </Text>
        ) : (
          messageSelected.destinatarios.map(item => (
            <Text style={styles.text}>{item.nombre},</Text>
          ))
        )}
      </View>
      <View style={styles.flex}>
        <Text style={styles.title}>Fecha: </Text>
        {messageSelected.fec_bne_fecha ? (
          <Text style={styles.text}>{messageSelected.fec_bne_fecha}</Text>
        ) : (
          <Text style={styles.text}>{messageSelected.fec_bns_fecha}</Text>
        )}
      </View>
      <View style={styles.flex}>
        <Text style={styles.title}>Mensaje: </Text>
        {messageSelected.str_bne_mensaje ? (
          <Text style={styles.text}>{messageSelected.str_bne_mensaje}</Text>
        ) : (
          <Text style={styles.text}>{messageSelected.str_bns_mensaje}</Text>
        )}
      </View>
      {messageSelected.detalles.length > 0 ? (
        <View style={styles.containerAnexos}>
          <Text style={styles.title}>Anexos: </Text>
          <View style={styles.flexButton}>
            {messageSelected.detalles.map(item => {
              return (
                <Button
                  onPress={() => openFile(item.str_bed_ruta)}
                  title="anexo"
                  color={colors.messagge}
                  accessibilityLabel="Learn more about this purple button"
                />
              );
            })}
          </View>
        </View>
      ) : null}
      <View style={styles.flexButton}>
        <Button
          title="REENVIAR"
          color={colors.messagge}
          onPress={() => reenviar()}
        />
        <Button
          title="RESPONDER"
          color={colors.messagge}
          onPress={() => responder()}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  flex: {
    flexDirection: 'row',
    marginVertical: 5,
  },
  title: {
    fontWeight: 'bold',
    fontSize: 18,
  },
  text: {
    fontSize: 18,
  },
  flexButton: {
    marginTop: 50,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  containerAnexos: {
    marginTop: 15,
  },
});
export default MessageDetail;
