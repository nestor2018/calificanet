import React, {useState, useEffect} from 'react';
import {
  View,
  Text,
  SafeAreaView,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {Root} from '@kyupss/native-popup';

import {events} from '../../store/events';
import ItemSeparator from '../../components/ItemSeparator';
import colors from '../../constants/colors';

const Item = ({item, onPress, backgroundColor, textColor, navigation}) => {
  return (
    <TouchableOpacity onPress={onPress} style={[styles.item, backgroundColor]}>
      <Text style={styles.title}>{item.str_eve_titulo}</Text>
      <View style={styles.containerItem}>
        <Text style={styles.item}>Inicio:</Text>
        <Text style={styles.item}>{item.fec_eve_inicio}</Text>
      </View>
      <View style={styles.containerItem}>
        <Text style={styles.item}>Fin:</Text>
        <Text style={styles.item}>{item.fec_eve_fin}</Text>
      </View>
      <View style={styles.containerDescription}>
        <Text style={styles.description}>{item.mem_eve_texto}</Text>
      </View>
    </TouchableOpacity>
  );
};

const Events = ({navigation}) => {
  const dispatch = useDispatch();
  const url = useSelector(state => state.colegioSelected.url);
  const token = useSelector(state => state.user.token);
  const uid_per = useSelector(state => state.user.user.info_usuario.perfil.uid);
  const eventsSelected = useSelector(state => state.events.events);
  const statusEvents = useSelector(state => state.events.status);

  let getEvents = () => {
    dispatch(
      events({
        token,
        url,
        uid_per,
      }),
    );
  };
  useEffect(() => {
    getEvents();
  }, []);
  const eventSelected = item => {
    console.log(item);
  };
  const renderItem = ({item}) => (
    <Item item={item} onPress={() => eventSelected(item)} />
  );
  return (
    <Root>
      <SafeAreaView>
        <View>
          {statusEvents == 'loading' ? (
            <View style={styles.containerLoading}>
              <ActivityIndicator size="large" color={colors.events} />
            </View>
          ) : null}
          <FlatList
            data={eventsSelected}
            renderItem={renderItem}
            keyExtractor={item => item.uid}
            ListEmptyComponent={<Text>No hay eventos disponibles</Text>}
            ItemSeparatorComponent={({highlighted}) => <ItemSeparator />}
          />
        </View>
      </SafeAreaView>
    </Root>
  );
};

const styles = StyleSheet.create({
  title: {
    fontWeight: 'bold',
    textAlign: 'center',
    marginVertical: 8,
  },
  containerItem: {
    flexDirection: 'row',
  },
  item: {
    paddingHorizontal: 5,
  },
  description: {
    paddingVertical: 5,
  },
  containerLoading: {
    marginTop: 50,
  },
});

export default Events;
