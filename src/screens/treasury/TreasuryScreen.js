import React, {useEffect, useState} from 'react';
import {SafeAreaView, View, Text, Button, StyleSheet} from 'react-native';
import {Root} from '@kyupss/native-popup';
import {useDispatch, useSelector} from 'react-redux';
import FileViewer from 'react-native-file-viewer';
import RNFS from 'react-native-fs';

import colors from '../../constants/colors';
import FilterHeader from '../../components/FilterHeader';
import {file} from '../../store/files';

const Treasury = ({navigation}) => {
  const dispatch = useDispatch();
  const uid_est = useSelector(state => state.user.studentSelected);
  const token = useSelector(state => state.user.token);
  const url = useSelector(state => state.colegioSelected.url);
  const fileTesoreria = useSelector(state => state.files.archivoTesoreria);
  const urlImages = useSelector(state => state.colegioSelected.urlImages);
  let getChores = () => {
    dispatch(
      file({
        url,
        token,
        uid_est,
      }),
    );
  };
  useEffect(() => {
    getChores();
  }, []);
  useEffect(() => {
    getChores();
  }, [uid_est]);
  const openFile = file => {
    console.log(file);
    const typeDocument = file.substr(-3);
    const path = `${urlImages}${file}`;
    const localFile = `${RNFS.DocumentDirectoryPath}/temporaryfile.${typeDocument}`;
    if (
      typeDocument == 'pdf' ||
      typeDocument == 'jpg' ||
      typeDocument == 'png'
    ) {
      const options = {
        fromUrl: path,
        toFile: localFile,
      };
      RNFS.downloadFile(options)
        .promise.then(() => FileViewer.open(localFile))
        .then(() => {
          // success
        })
        .catch(error => {
          // error
        });
    } else {
      dispatch(changeUrl(path));
      dispatch(changeTitle('title'));
      navigation.navigate('Browser');
    }
  };
  return (
    <SafeAreaView>
      <View style={styles.flex}>
        <FilterHeader type="one" bgColor={colors.blue} />
        <View style={styles.containerButton}>
          <Button
            onPress={() => openFile(fileTesoreria)}
            title="Ver archivo"
            color={colors.blue}
            accessibilityLabel="Learn more about this purple button"
          />
        </View>
      </View>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  flex: {},
  containerButton: {
    paddingTop: 20,
  },
});

export default Treasury;
