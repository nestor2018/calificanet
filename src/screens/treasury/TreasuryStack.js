import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import TreasuryScreen from './TreasuryScreen';
import colors from '../../constants/colors';
import BrowserScreen from '../browser/Browser';

const TreasuryStack = ({navigation}) => {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Tesoreria"
        component={TreasuryScreen}
        options={{
          title: 'TESORERIA',
          headerStyle: {backgroundColor: colors.blue},
          headerLeftContainerStyle: {paddingLeft: 20},
          headerTintColor: `${colors.white}`,
          headerLeft: () => (
            <FontAwesome5
              name={'bars'}
              size={22}
              color={colors.white}
              onPress={() => navigation.openDrawer()}
            />
          ),
        }}
      />
      <Stack.Screen
        name="Browser"
        component={BrowserScreen}
        options={{
          headerTintColor: `${colors.white}`,
          title: ``,
          headerBackTitleVisible: false,
          handlerLeft: false,
        }}
      />
    </Stack.Navigator>
  );
};

export default TreasuryStack;
