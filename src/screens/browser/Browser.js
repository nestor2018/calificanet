import React, {Component} from 'react';
import {WebView} from 'react-native-webview';
import {useSelector} from 'react-redux';

const Browser = ({navigation}) => {
  setTimeout(() => {
    console.log('----');
    navigation.goBack();
  }, 5);
  const url = useSelector(state => state.files.url);
  console.log(url);
  return (
    <WebView
      source={{
        uri: url,
      }}
    />
  );
};

export default Browser;
