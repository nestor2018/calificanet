import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import BrowserScreen from './Browser';
import colors from '../../constants/colors';

const NewsStack = ({navigation}) => {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Browser"
        component={BrowserScreen}
        options={{
          title: 'titulo',
          headerStyle: {backgroundColor: colors.news},
          headerTintColor: `${colors.white}`,
          headerLeftContainerStyle: {paddingLeft: 20},
          headerLeft: () => (
            <FontAwesome5
              name={'bars'}
              color={colors.white}
              size={22}
              onPress={() => navigation.openDrawer()}
            />
          ),
        }}
      />
    </Stack.Navigator>
  );
};

export default NewsStack;
